<?php

namespace App\Libraries;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use Mockery\Exception;

class ApiClientLibrary
{
    protected $BASE_URL;
    protected $MK_API_KEY;
    protected $MK_API_SECRET;

    public function __construct()
    {
        $this->BASE_URL = env('MASSKREDIT_BASE_URL');
        $this->MK_API_KEY = env('MASSKREDIT_API_KEY');
        $this->MK_API_SECRET = env('MASSKREDIT_API_SECRET');
    }

    public function hardCodedContent()
    {
        $content = [
            "fullname" => "John Doe",
            "productName" => "HP Oppo A73",
            "amount" => 3600000,
            "phoneNumber" => "082320072980"
        ];

        $endpoint = '/v2/submission/sms/purchase-order';

        $response = $this->request($content, $endpoint);

        return $response;
    }

    public function request(array $content, $endpoint)
    {
        try {
            $url = $this->BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->post($url, [
                'headers' => [
                    'api_key' => $this->MK_API_KEY,
                    'api_secret' => $this->MK_API_SECRET,
                    'signature' => $this->signature($this->MK_API_KEY, $this->MK_API_SECRET, json_encode($content)),
                    'Content-Type' => 'application/json'
                ],
                'json' => $content
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            return $responseJSON;
        } catch (ClientException $exception) {
            return null;
        }
    }

    public function signature($api_key, $api_secret, $content)
    {
        try {
            $body = hash('sha256', $content);
            $composed =
                "AUTH/auth?api_key=" . $api_key .
                "&api_secret=" . $api_secret .
                "&auth_version=1.0" .
                "&body_md5=" . $body;

            $hash = hash_hmac('sha256', $composed, $api_secret);
            return $hash;
        } catch (Exception $exception) {
            echo 'ERROR: ' . $exception->getMessage();
            return null;
        }
    }
}