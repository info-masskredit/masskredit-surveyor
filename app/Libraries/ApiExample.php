<?php

namespace App\Libraries;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class ApiExample
{
    protected $ZENDESK_BASE_URL;
    protected $ZENDESK_EMAIL;
    protected $ZENDESK_PASSWORD;
    protected $BASE64_CREDENTIAL;
    private $PRIORITY;
    private $STATUS;

    public function __construct()
    {
        $this->ZENDESK_BASE_URL = env('ZENDESK_BASE_URL');
        $this->ZENDESK_EMAIL = env('ZENDESK_EMAIL');
        $this->ZENDESK_PASSWORD = env('ZENDESK_PASSWORD');
        $this->BASE64_CREDENTIAL = base64_encode($this->ZENDESK_EMAIL . ':' . $this->ZENDESK_PASSWORD);

        $this->PRIORITY = [
            1 => 'low',
            2 => 'normal',
            3 => 'high',
            4 => 'urgent'
        ];

        $this->STATUS = [
            1 => 'new',
            2 => 'pending',
            3 => 'open',
            4 => 'solved'
        ];
    }

    private function GET($endpoint)
    {
        try {
            $url = $this->ZENDESK_BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->get($url, [
                'headers' => [
                    'Authorization' => 'Basic ' . $this->BASE64_CREDENTIAL
                ]
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    private function POST($endpoint, $content)
    {
        try {
            $url = $this->ZENDESK_BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->post($url, [
                'headers' => [
                    'Authorization' => 'Basic ' . $this->BASE64_CREDENTIAL,
                    'Content-Type' => 'application/json'
                ],
                'json' => $content
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    private function PUT($endpoint, $content)
    {
        try {
            $url = $this->ZENDESK_BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->put($url, [
                'headers' => [
                    'Authorization' => 'Basic ' . $this->BASE64_CREDENTIAL,
                    'Content-Type' => 'application/json'
                ],
                'json' => $content
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    private function DELETE($endpoint)
    {
        try {
            $url = $this->ZENDESK_BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->delete($url, [
                'headers' => [
                    'Authorization' => 'Basic ' . $this->BASE64_CREDENTIAL
                ]
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function listLocales()
    {
        return $this->GET('locales.json');
    }

    public function listUsers()
    {
        return $this->GET('users.json');
    }

    public function listTickets()
    {
        return $this->GET('tickets.json');
    }

    public function showTicket($id)
    {
        return $this->GET('tickets/'.$id.'.json');
    }

    public function showUser($id)
    {
        return $this->GET('users/'.$id.'.json');
    }

    public function createTicketAsCredential($subject, $priority, $comment = null)
    {
        $content = [];
        $content['ticket']['subject'] = $subject;
        $content['ticket']['comment']['body'] = $comment;
        $content['ticket']['priority'] = $this->PRIORITY[$priority];
        return $this->POST('tickets.json', $content);
    }

    public function createTicket($subject, $comment, $name, $email, $local = 1)
    {
        $content = [];
        $content['ticket']['subject'] = $subject;
        $content['ticket']['comment']['body'] = $comment;
        $content['ticket']['requester']['locale_id'] = $local;
        $content['ticket']['requester']['name'] = $name;
        $content['ticket']['requester']['email'] = $email;
        return $this->POST('tickets.json', $content);
    }

    public function updateTicket($id, $status, $comment = null, $public = null)
    {
        $content = [];
        $comment ? $content['ticket']['comment']['body'] = $comment : '';
        $comment ? $content['ticket']['comment']['public'] = $public : '';
        $content['ticket']['status'] = $this->STATUS[$status];
        return $this->PUT('tickets/'.$id.'.json', $content);
    }

    public function deleteTicket($id)
    {
        return $this->DELETE('tickets/'.$id.'.json');
    }

    public function datatablesTickets()
    {
        $tickets = new Collection();
        $responses = $this->listTickets()['tickets'];

        foreach ($responses as $response) {
            $user = $this->showUser($response['requester_id'])['user'];
            $tickets->push((object)[
                'id' => $response['id'],
                'subject' => $response['subject'],
                'type' => $response['type'],
                'priority' => $response['priority'],
                'status' => $response['status'],
                'requester' => $user['name']
            ]);
        }

        return $tickets;
    }
}