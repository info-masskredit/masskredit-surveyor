<?php

namespace App\Libraries;


class ApiResponseLibrary
{
    protected $LIMIT = 10;
    public function listPaginate($collection)
    {
        $return = [];
        $paginated = $collection->paginate($this->LIMIT);
        $return['meta']['code'] = 200;
        $return['meta']['message'] = trans('message.api.success');
        $return['meta']['total'] = $paginated->total();
        $return['meta']['per_page'] = $paginated->perPage();
        $return['meta']['current_page'] = $paginated->currentPage();
        $return['meta']['last_page'] = $paginated->lastPage();
        $return['meta']['has_more_pages'] = $paginated->hasMorePages();
        $return['meta']['from'] = $paginated->firstItem();
        $return['meta']['to'] = $paginated->lastItem();
        $return['links']['self'] = url()->full();
        $return['links']['next'] = $paginated->nextPageUrl();
        $return['links']['prev'] = $paginated->previousPageUrl();
        $return['data'] = $paginated->items();
        return $return;
    }

    public function singleData($data, array $relations)
    {
        $return = [];
        $return['meta']['code'] = 200;
        $return['meta']['message'] = trans('message.api.success');
        $return['data'] = $data;
        $return = $this->generateRelations($return, $relations);
        return $return;
    }

    private function generateRelations($return, $relations) {
        if (isset($relations)) {
            foreach ($relations as $key => $relation)
            {
                $return['data'][$key] = $relation;
            }
        }
        return $return;
    }

    public function successResponse($id)
    {
        $return = [];
        $return['meta']['code'] = 200;
        $return['meta']['message'] = trans('message.api.success');
        $return['data']['id'] = $id;
        return $return;
    }

    public function errorResponse(\Exception $e)
    {
        $return = [];
        $return['meta']['code'] = 500;
        $return['meta']['message'] = trans('message.api.error');
        $return['meta']['error'] = $e->getMessage();
        return $return;
    }

    public function validationFailResponse($errors)
    {
        $return = [];
        $return['meta']['code'] = 400;
        $return['meta']['message'] = trans('message.api.error');
        $return['data'] = $errors;
        return $return;
    }
}