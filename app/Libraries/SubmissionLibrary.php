<?php
/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 21/07/18
 * Time: 20.01
 */

namespace App\Libraries;

use Illuminate\Support\Collection;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class SubmissionLibrary
{

    protected $BASE_URL;
    protected $MK_API_KEY;


    public function __construct()
    {
        $this->BASE_URL = env('MASSKREDIT_BASE_URL');
        $this->MK_API_KEY = env('MASSKREDIT_API_KEY');

    }

    private function GET($endpoint)
    {
        try {
            $url = $this->BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->get($url, [
                'headers' => [
                    'api_key' => $this->MK_API_KEY,
                    'Content-Type' => 'application/json'
                ]
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            Log::debug($responseJSON);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    private function POST($endpoint, $content)
    {
        try {
            $url = $this->BASE_URL . $endpoint;
            $client = new Client();
            $response = $client->post($url, [
                'headers' => [
                    'api_key' => $this->MK_API_KEY,
                    'Content-Type' => 'application/json'
                ],
                'json' => $content
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            Log::debug($responseJSON);
            return $responseJSON;
        } catch (ClientException $exception) {
            return $exception->getMessage();
        }
    }

    public function listsurveydata()
    {
        return $this->GET('admin/surveyor/list');
    }

    public function showSubmission($id_submission)
    {
        $details = [];
        $submission = (object) $this->GET('admin/surveyor/'.$id_submission)['data'];

        $details['data'] = $submission;

        return (object) $details;
    }

    public function editSubmission($id_submission)
    {
        $content = [];
        $content['id_submission'] = $id_submission;
//        $this->POST('admin/surveyor/remove', $content);
        $details = [];
        $submission = (object) $this->GET('admin/surveyor/'.$id_submission)['data'];

        $details['data'] = $submission;

        return (object) $details;
    }

    public function datatablesurvey()
    {
        $surveyData = new Collection();
        $responses = $this->listsurveydata()['data']['survey'];

        foreach ($responses as $response) {

            $surveyData->push((object)[
                'id_submission' => $response['id_submission'],
                'user_name' => $response['user_name'],
                'user_nik' => $response['user_nik'],
                'user_employment' => $response['user_employment'],
                'user_phone_number' => $response['user_phone_number'],
                'user_work_period' => $response['user_work_period'],
                'user_placement_location' => $response['user_placement_location'],
                'company_hrd_name' => $response['company_hrd_name'],
                'company_industry' => $response['company_industry'],
                'company_call_number' => $response['company_call_number'],
                'company_bank_payroll' => $response['company_bank_payroll'],
                'interviewee_name' => $response['interviewee_name'],
                'interviewee_employment' => $response['interviewee_employment'],
                'status_name' => $response['status_name'],
                'surveyor_id' => $response['surveyor_id'],
                'status_id' => $response['status_id'],
                'province_id' => $response['province_id'],
                'city_id' => $response['city_id'],
                'district_id' => $response['district_id'],
                'city_name' => $response['city_name'],
                'district_name' => $response['district_name'],
                'is_offline' => $response['is_offline'],

            ]);
        }

        return $surveyData;
    }


}