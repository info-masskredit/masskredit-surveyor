<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FillSurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_id' => 'required|exists:surveys,id',
            'user_name' => 'required',
            'user_nik' => 'required|digits_between:15,16',
            'user_employment' => 'required',
            'user_phone_number' => 'required',
            'user_work_period' => 'required|numeric',
            'user_placement_location' => 'required',
            'company_hrd_name' => 'required',
            'company_industry' => 'required',
            'company_call_number' => 'required',
            'company_bank_payroll' => 'required',
            'interviewee_name' => 'required',
            'interviewee_employment' => 'required',
            'photo_location' => 'required|exists:images,id',
            'photo_interviewee' => 'required|exists:images,id',
            'survey_empty_reason' => 'nullable',
        ];
    }
}
