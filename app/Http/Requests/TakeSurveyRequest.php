<?php

namespace App\Http\Requests;

use App\Models\Status;
use Illuminate\Foundation\Http\FormRequest;

class TakeSurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_id' => 'required|exists:surveys,id',
            'survey_status' => 'required|exists:status,id|in:'.$this->fieldStatus(),
        ];
    }


    /**
     * Get status ids for field type.
     *
     * @return string
     */
    private function fieldStatus()
    {
        return implode(Status::$fieldStatusForMobile, ',');
    }
}
