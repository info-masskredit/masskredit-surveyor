<?php

namespace App\Http\Controllers\Api;

use App\Libraries\ApiClientLibrary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\ApiResponseLibrary;
use Validator;

class MerchantController extends Controller
{
    protected $menu;
    protected $route;
    protected $slug;
    protected $model;
    protected $apiLib;

    protected $bankModel;
    protected $userModel;
    protected $roleModel;
    protected $addressModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu         = trans('menu.merchant.name');
        $this->route        = $this->routes['api'].'merchant';
        $this->slug         = $this->slugs['api'].'merchant';

        $this->model = new \App\Models\Merchant;
        $this->apiLib = new ApiResponseLibrary;
        $this->bankModel = new \App\Models\Bank;
        $this->userModel = new \App\Models\User;
        $this->roleModel = new \App\Models\Role;
        $this->addressModel = new \App\Models\Address;
    }

    public function index()
    {
        try {
            $sql = $this->model->sql();
            $return = $this->apiLib->listPaginate($sql);
            return response($return, 200);
        } catch (\Exception $e) {
            return response($this->apiLib->errorResponse($e), 500);
        }
    }

    public function detail($id)
    {
        try {
            $data = $this->model->findOrFail($id);
            return response($this->apiLib->singleData($data, [
                'user' => $data->user,
                'address' => $data->address,
                'bank' => $data->bank
            ]), 200);
        } catch (\Exception $e) {
            return response($this->apiLib->errorResponse($e), 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'phone' => 'required|max:255',
                'bank' => 'required',
                'address' => 'required|max:255',
                'email' => 'required|max:255|email|unique:'.$this->userModel->table.',email,NULL,id,deleted_at,NULL',
                'password' => 'required|min:8|confirmed|regex:'.regex_password_rules(),
            ], [
                'regex' => 'The :attribute format must be contained:'.regex_password_messages()
            ]);

            if ($validator->fails()) {
                return response($this->apiLib->validationFailResponse($validator->errors()), 400);
            }

            $role = $this->roleModel->where('slug', 'merchant')->first();

            $address = $this->addressModel->create([
                'address' => $request->address,
            ]);

            $user = $this->userModel->create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password)
            ])->user_role()->create([
                'role_id'   => $role->id
            ]);

            $data = $this->model;
            $data->user_id = $user->id;
            $data->address_id = $address->id;
            $data->bank_id = $request->bank;
            $data->phone = $request->phone;
            $data->password = bcrypt($request->password);
            $data->created_by = auth()->user()->id;
            $data->save();

            return response($this->apiLib->successResponse($data->id), 200);
        } catch (\Exception $e) {
            return response($this->apiLib->errorResponse($e), 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $this->model->findOrFail($id);

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'bank' => 'required',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'email' => 'required|max:255|email|unique:'.$this->userModel->table.',email,'.$data->user_id.',id,deleted_at,NULL',
                'password' => 'min:8|confirmed|regex:'.regex_password_rules(),
            ], [
                'regex' => 'The :attribute format must be contained:'.regex_password_messages()
            ]);

            if ($validator->fails()) {
                return response($this->apiLib->validationFailResponse($validator->errors()), 400);
            }

            $data->update([
                'phone' => $request->phone,
                'bank_id' => $request->bank
            ]);

            $user = $this->userModel->find($data->user_id);

            $user->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password)
            ]);

            $address = $this->addressModel->find($data->address_id);

            $address->update([
                'address'      => $request->address,
            ]);

            return response($this->apiLib->successResponse($id), 200);
        } catch (\Exception $e) {
            return response($this->apiLib->errorResponse($e), 500);
        }
    }

    public function destroy($id)
    {
        try {
            $data = $this->model->findOrFail($id);
            $data->delete();
            return response($this->apiLib->successResponse($id), 200);
        } catch (\Exception $e) {
            return response($this->apiLib->errorResponse($e), 500);
        }
    }
}
