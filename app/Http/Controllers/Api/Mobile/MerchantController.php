<?php

namespace App\Http\Controllers\Api\Mobile;

use App\Libraries\ApiResponseLibrary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MerchantController extends Controller
{
    public function getProfile(Request $request)
    {
        return response((new ApiResponseLibrary())->singleData($request->user()->merchant, []), 200);
    }
}
