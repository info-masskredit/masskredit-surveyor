<?php

namespace App\Http\Controllers\Api\Mobile;

use App\Libraries\ApiResponseLibrary;
use App\models\Merchant;
use App\Models\Surveyor;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use \Laravel\Passport\Http\Controllers\AccessTokenController as ParentAccesstokenController;
use Lcobucci\JWT\Parser;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;

class AccessTokenController extends ParentAccesstokenController
{
    use ValidatesRequests;

    /**
     * Authorize a client to access the user's account.
     *
     * @param  ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ValidationException
     */
    public function issueToken(ServerRequestInterface $request)
    {
        $body = $request->getParsedBody();

        $this->validate($body, ['fcm_token' => 'required']);

        $response = parent::issueToken($request);


        if ($response->getStatusCode() === Response::HTTP_OK) {
            $this->insertFcmTokenToMerchant($body['username'], $body['fcm_token']);
        }

        return $response;
    }

    /**
     * Revoke an access token.
     * @param  Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function revokeToken(Request $request)
    {
        $id = (new Parser())->parse($request->bearerToken())->getHeader('jti');
        $request->user()->tokens->find($id)->revoke();

        return response((new ApiResponseLibrary())->singleData([], []), 200);
    }

    /**
     * Validate the given request with the given rules.
     *
     * @param  array $data
     * @param  array $rules
     * @param  array $messages
     * @param  array $customAttributes
     * @return void
     * @throws ValidationException
     */
    public function validate($data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($data, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new ValidationException($validator,
                new JsonResponse($this->formatValidationErrors($validator), 422)
            );
        }
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param  string  $phone
     * @param  string  $fcm_token
     * @return void
     */
    private function insertFcmTokenToMerchant($phone, $fcm_token)
    {
        Surveyor::where('phone_number', $phone)->update(['fcm_token' => $fcm_token]);
    }
}
