<?php
/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 01/08/18
 * Time: 15.47
 */

namespace App\Http\Controllers\Api\Mobile;

use App\Http\Requests\FillSurveyRequest;
use App\Http\Requests\TakeSurveyRequest;
use App\Libraries\ApiResponseLibrary;
use App\Models\Status;
use App\Models\Survey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class SurveyorController extends Controller
{
    /**
     * Get surveyor profile.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getProfile(Request $request)
    {
        return response((new ApiResponseLibrary())->singleData($request->user()->surveyor, []), 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getListSurvey(Request $request)
    {
        return response((new ApiResponseLibrary())->singleData($request->user()->survey_field->where('status_id', 12), []), 200);
    }

    /**
     * @param FillSurveyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fillSurvey(FillSurveyRequest $request)
    {
        $survey = Survey::find($request->input('survey_id'));
        $survey->fill($request->all());
        $survey->save();

        $response = [
            'meta' => [
                'error' => 0,
                'code' => 200,
                'result' => 'success',
                'message' => 'Fill survey berhasil'
            ],
            'data' => (object)[]
        ];

        return response()->json($response);
    }

    /**
     * @param TakeSurveyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function takeSurvey(TakeSurveyRequest $request)
    {
        $survey = Survey::find($request->input('survey_id'));

        $status = Status::find($request->input('survey_status'));

        $response = [
            'meta' => [
                'error' => 0,
                'code' => 201,
                'result' => 'success',
                'message' => 'Take survey berhasil'
            ],
            'data' => (object)[]
        ];

        DB::beginTransaction();

        try {

            $survey->fill([
                'work_location_lat' => $request->input('lat'),
                'work_location_lon' => $request->input('lon')
            ]);

            $survey->status()->associate($status);

            $survey->save();

        } catch (\Exception $exception) {

            DB::rollBack();

            $response['meta']['error'] = 1;
            $response['meta']['code'] = 500;
            $response['meta']['result'] = 'error';
            $response['meta']['message'] = 'Data cannot be stored to database.';

        }

        DB::commit();

        return response()->json($response, $response['meta']['code']);
    }
}