<?php

namespace App\Http\Controllers\Api\Mobile;

use App\Http\Requests\StoreImagesRequest;
use App\Models\Image;
use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreImagesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreImagesRequest $request)
    {
        $files = $request->file();

        $images = collect();

        foreach ($files as $key => $file) {
            $path = $file->storeAs(
                'images' . DIRECTORY_SEPARATOR . 'surveys', str_random().'.'.$request->{$key}->extension(), 'public'
            );

            $image = Image::create(['image' => $path]);

            $image->field_name = $key;

            $images->push($image);
        }

        $response = [
            'meta' => [
                'error' => 0,
                'code' => 201,
                'result' => 'success',
                'message' => 'Upload image berhasil'
            ],
            'data' => $images
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Image $image
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Image $image)
    {
        $image->delete();

        $response = [
            'meta' => [
                'error' => 0,
                'code' => 200,
                'result' => 'success',
                'message' => 'Delete image berhasil'
            ],
            'data' => (object)[]
        ];

        return response()->json($response);
    }
}
