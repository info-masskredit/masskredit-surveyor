<?php

namespace App\Http\Controllers\Backend;

use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use Illuminate\Support\Facades\DB;
use Validator;

/**
 * @property \App\Models\Surveyor model
 */
class SurveyorController extends Controller
{
    private $provinceModel;
    private $cityModel;
    private $districtModel;
    private $surveyModel;
    private $userModel;
    private $roleModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu = trans('menu.surveyor.name');
        $this->route = $this->routes['backend'] . 'surveyor';
        $this->route2 = $this->routes['backend'] . 'survey';
        $this->slug = $this->slugs['backend'] . 'surveyor';
        $this->view = $this->views['backend'] . 'surveyors';
        $this->breadcrumb = '<li><a href="' . route($this->route . '.index') . '">' . $this->menu . '</a></li>';
        # share parameters
        $this->share();
        # init model
        $this->model = new \App\Models\Surveyor;
        $this->surveyModel = new \App\Models\Survey;
        $this->provinceModel = new \App\Models\Province;
        $this->cityModel = new \App\Models\City;
        $this->districtModel = new \App\Models\District;
        $this->userModel        = new \App\Models\User;
        $this->roleModel        = new \App\Models\Role;
    }

    public function index()
    {
        try {
            $breadcrumb = $this->breadcrumbs($this->breadcrumb . '<li class="active">' . trans('label.view') . '</li>');
            return view($this->view . '.index', compact('breadcrumb'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables()
    {
        try {
            $data = numrows($this->model->sql()->get());
            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    $action = null;
                    if (check_access('detail', $this->slug)) {
                        $action .= '<a data-href="' . route($this->route . '.detail', encodeids($data->id)) . '" class="btn btn-xs btn-success btn-modal-action" title="' . trans('label.detail') . '" data-title="' . trans('form.detail', ['menu' => $this->menu]) . '" data-icon="fa fa-search fa-fw" data-background="modal-success">' . trans('icon.detail') . '</a>';
                    }
                    if (check_access('update', $this->slug)) {
                        $action .= '<a data-href="' . route($this->route . '.edit', encodeids($data->id)) . '" class="btn btn-xs btn-warning btn-modal-form" title="' . trans('label.edit') . '" data-title="' . trans('form.edit', ['menu' => $this->menu]) . '" data-icon="fa fa-edit fa-fw" data-background="modal-warning">' . trans('icon.edit') . '</a>';
                    }
                    if (check_access('delete', $this->slug)) {
                        $action .= '<a data-href="' . route($this->route . '.delete', encodeids($data->id)) . '" class="btn btn-xs btn-danger btn-modal-action" title="' . trans('label.delete') . '" data-title="' . trans('form.delete', ['menu' => $this->menu]) . '" data-icon="fa fa-trash-o fa-fw" data-background="modal-danger">' . trans('icon.delete') . '</a>';
                    }
                    return $action;
                })
                ->make(true);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables2($id)
    {
        try {
            $data = numrows($this->surveyModel->surveylist()->where('surveys.surveyor_id', $id)->get());
            return Datatables::of($data)
                ->make(true);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function detail($id)
    {
//        $assign = DB::table('assigns')->where('surveyor_id', decodeids($id))->pluck('survey_id');
//        foreach ($assign as $survey_id){
//            echo $survey_id;
//        }
//
//        dd($survey_id);

        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);

            return view($this->view . '.form.detail', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        try {
            $districts = $this->districtModel->all();
            $cities = $this->cityModel->all();
            $provinces = $this->provinceModel->all();
            return view($this->view . '.form.create', compact('provinces', 'cities', 'districts'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'phone_number' => 'required|max:255',
                'nik' => 'required|max:255',
                'province' => 'required|max:255',
                'city' => 'required|max:255',
                'district' => 'required|max:255',
                'email' => 'required|max:255|email|unique:' . $this->model->table . ',email,NULL,id,deleted_at,NULL',
                'password' => 'required|min:8|confirmed',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route . '.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            $role = $this->roleModel->where('slug', 'surveyor')->first();

            $user = $this->userModel->create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password)
            ])->user_role()->create([
                'role_id'   => $role->id
            ]);

            $data = $this->model;
            $data->name = $request->name;
            $data->phone_number = $request->phone_number;
            $data->nik = $request->nik;
            $data->province_id = $request->province;
            $data->city_id = $request->city;
            $data->district_id = $request->district;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            $data->latitude = -6.225014;
            $data->longitude = 106.900447;
            $data->max_survey = $request->max_survey;
            $data->user_id = $user->id;
            $data->created_by = auth()->user()->id;
            $data->save();

            action_message('create', $this->menu);
            return json_encode(['redirect' => route($this->route . '.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function edit($id)
    {
        try {
            $id = decodeids($id);
            $districts = $this->districtModel->all();
            $cities = $this->cityModel->all();
            $provinces = $this->provinceModel->all();
            $data = $this->model->findOrFail($id);
            return view($this->view . '.form.edit', compact('data', 'provinces',
                'cities', 'districts'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function update(Request $request, $id)
    {
//        try {
        $id = decodeids($id);
        $data = $this->model->findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone_number' => 'required|max:255',
            'nik' => 'required|max:255',
            'province' => 'required|max:255',
            'city' => 'required|max:255',
            'district' => 'required|max:255',
            'email' => 'required|max:255|email',
            'password' => 'min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route($this->route . '.edit', encodeids($id))
                ->withErrors($validator)
                ->withInput();
        }


        $data->name = $request->name;
        $data->phone_number = $request->phone_number;
        $data->nik = $request->nik;
        $data->province_id = $request->province;
        $data->city_id = $request->city;
        $data->district_id = $request->district;
        $data->email = $request->email;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->max_survey = $request->max_survey;
        $data->update();


        action_message('update', $this->menu);
        return json_encode(['redirect' => route($this->route . '.index')]);
//        } catch (\Exception $e) {
//            abort(500);
//        }
    }

    public function delete($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view . '.form.delete', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function destroy($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->delete();
            action_message('delete', $this->menu);
            return redirect()->route($this->route . '.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
