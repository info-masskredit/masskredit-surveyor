<?php
/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 21/07/18
 * Time: 20.25
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Libraries\SubmissionLibrary;
use App\Models\Surveyor;
use Illuminate\Http\Request;

use Datatables;
use Illuminate\Support\Facades\DB;

/**
 * @property \App\Models\Survey model
 * @property \App\Models\SurveyByPhone phoneModel
 * @property \App\Models\SurveyByField fieldModel
 */
class SubmissionController extends Controller
{
    protected $submissionLib;
    private $surveyorModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu = 'Submission';
        $this->route = $this->routes['backend'] . 'submission';
        $this->routeSurvey = $this->routes['backend'] . 'survey';
        $this->slug = $this->slugs['backend'] . 'submission';
        $this->view = $this->views['backend'] . 'submission';
        $this->breadcrumb = '<li><a href="' . route($this->route . '.index') . '">' . $this->menu . '</a></li>';
        # share parameters
        $this->share();
        # init model
        $this->model = new \App\Models\Survey;
        $this->phoneModel = new \App\Models\SurveyByPhone;
        $this->fieldModel = new \App\Models\SurveyByField;
        $this->assignModel = new \App\Models\Assign;
        $this->submissionLib = new SubmissionLibrary;

    }

    public function index()
    {
        try {
            $breadcrumb = $this->breadcrumbs($this->breadcrumb . '<li class="active">' . trans('label.view') . '</li>');
            return view($this->view . '.index', compact('breadcrumb'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        try {
            return view($this->view . '.form.create');
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {

        $surveyorData = DB::table('surveyors')->where('city_id', $request->city_id)->latest('max_survey')->value('max_survey');

        if($surveyorData != null) {
            $surveyorId = DB::table('surveyors')->where([['city_id', '=' , $request->city_id],
                ['assigned_survey', '<' , $surveyorData], ])->value('id');
        } else {
            $surveyorId = null;
        }


        if($surveyorId != null){
            $count = DB::table('assigns')->where('surveyor_id', $surveyorId)->count();
        } else {
            $count = 0;
        }

//        debug($surveyorData, true);
//        $nama = DB::table('assigns')->where('surveyor_id', $surveyorId)->value('id');

//        $surveyorIdNext = DB::table('surveyors')->where('city_id', $request->city_id)->latest()->value('id');

        if ($surveyorId != null) {

            $this->surveyorModel = Surveyor::where('id', $surveyorId)->first();
        }


//        try {

        if($surveyorId != null){

            $data3 = $this->surveyorModel;
            $data3->assigned_survey = $count+1;
            $data3->save();
        }

        $data = $this->model;
        $data->user_name = $request->user_name;
        $data->user_nik = $request->user_nik;
        $data->user_employment = $request->user_employment;
        $data->user_phone_number = $request->user_phone_number;
        $data->user_work_period = $request->user_work_period;
        $data->user_placement_location = $request->user_placement_location;
        $data->company_hrd_name = $request->company_hrd_name;
        $data->company_industry = $request->company_industry;
        $data->company_call_number = $request->company_call_number;
        $data->company_bank_payroll = $request->company_bank_payroll;
        $data->interviewee_name = $request->interviewee_name;
        $data->interviewee_employment = $request->interviewee_employment;
        $data->status_name = $request->status_name;
        if ($surveyorId != null){
            $data->user_id = $this->surveyorModel->user_id;
        } else {
            $data->user_id = null;
        }


        if($surveyorId != null) {

            if($data3->assigned_survey > $data3->max_survey) {
                $data->surveyor_id = null;
            } else {
                $data->surveyor_id = $surveyorId;
            }
        }

        $data->status_id = 1;
        $data->province_id = $request->province_id;
        $data->city_id = $request->city_id;
        $data->district_id = $request->district_id;
        $data->econ_name = $request->econ_name;
        $data->econ_address = $request->econ_address;
        $data->econ_phone = $request->econ_phone;
        $data->econ_relationship = $request->econ_relationship;
        $data->proof_of_salary = $request->proof_of_salary;
        $data->is_offline = $request->is_offline;
        $data->work_location = $request->work_location;
        $data->save();

        $data1 = $this->phoneModel;
        $data1->user_name = $request->user_name;
        $data1->user_nik = $request->user_nik;
        $data1->user_employment = $request->user_employment;
        $data1->user_phone_number = $request->user_phone_number;
        $data1->user_work_period = $request->user_work_period;
        $data1->user_placement_location = $request->user_placement_location;
        $data1->company_hrd_name = $request->company_hrd_name;
        $data1->company_industry = $request->company_industry;
        $data1->company_call_number = $request->company_call_number;
        $data1->company_bank_payroll = $request->company_bank_payroll;
        $data1->interviewee_name = $request->interviewee_name;
        $data1->interviewee_employment = $request->interviewe_employment;
        $data1->status_name = $request->status_name;
        $data1->surveyor_id = $surveyorId;
        $data1->status_id = 1;
        $data1->province_id = $request->province_id;
        $data1->city_id = $request->city_id;
        $data1->district_id = $request->district_id;
        $data1->econ_name = $request->econ_name;
        $data1->econ_address = $request->econ_address;
        $data1->econ_phone = $request->econ_phone;
        $data1->econ_relationship = $request->econ_relationship;
        $data1->proof_of_salary = $request->proof_of_salary;
        $data1->is_offline = $request->is_offline;
        $data1->work_location = $request->work_location;
        if ($surveyorId != null){
            $data1->user_id = $this->surveyorModel->user_id;
        } else {
            $data1->user_id = null;
        }
        $data1->save();

        $data2 = $this->fieldModel;
        $data2->user_name = $request->user_name;
        $data2->user_nik = $request->user_nik;
        $data2->user_employment = $request->user_employment;
        $data2->user_phone_number = $request->user_phone_number;
        $data2->user_work_period = $request->user_work_period;
        $data2->user_placement_location = $request->user_placement_location;
        $data2->company_hrd_name = $request->company_hrd_name;
        $data2->company_industry = $request->company_industry;
        $data2->company_call_number = $request->company_call_number;
        $data2->company_bank_payroll = $request->company_bank_payroll;
        $data2->interviewee_name = $request->interviewee_name;
        $data2->interviewee_employment = $request->interviewe_employment;
        $data2->status_name = $request->status_name;
        $data2->surveyor_id = $surveyorId;
        $data2->status_id = 1;
        $data2->province_id = $request->province_id;
        $data2->city_id = $request->city_id;
        $data2->district_id = $request->district_id;
        $data2->econ_name = $request->econ_name;
        $data2->econ_address = $request->econ_address;
        $data2->econ_phone = $request->econ_phone;
        $data2->econ_relationship = $request->econ_relationship;
        $data2->proof_of_salary = $request->proof_of_salary;
        $data2->is_offline = $request->is_offline;
        $data2->work_location = $request->work_location;
        if ($surveyorId != null){
            $data2->user_id = $this->surveyorModel->user_id;
        } else {
            $data2->user_id = null;
        }
        $data2->save();

        $assign = $this->assignModel;
        $assign->survey_id = $data->id;
        $assign->surveyor_id = $surveyorId;
        $assign->save();

        action_message('move', $this->menu);
        return json_encode(['redirect' => route($this->routeSurvey . '.index')]);
//        } catch (\Exception $e) {
//            abort(500);
//        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);

//            $validator = Validator::make($request->all(), [
//                'name' => 'required|max:255',
//                'code' => 'required|digits:3',
//            ]);
//
//            if ($validator->fails()) {
//                return redirect()
//                    ->route($this->route.'.edit', encodeids($id))
//                    ->withErrors($validator)
//                    ->withInput();
//            }

            $data->user_name = $request->user_name;

            $data->save();

            action_message('update', $this->menu);
            return json_encode(['redirect' => route($this->route . '.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function detail($id_submission)
    {
        try {
            $id = decodeids($id_submission);
            $data = (object)$this->submissionLib->showSubmission($id);
//            dd($data);
            return view($this->view . '.form.detail', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function edit($id_submission)
    {
        try {
            $id_submission = decodeids($id_submission);
            $data = (object)$this->submissionLib->editSubmission($id_submission);
            return view($this->view . '.form.edit', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables()
    {
//        try {
        $data = numrows($this->submissionLib->datatablesurvey());
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                $action = null;
//                if (check_access('detail', $this->slug)) {
//                    $action .= '<a data-href="' . route($this->route . '.detail', encodeids($data->id_submission)) . '" class="btn btn-xs btn-success btn-modal-action" title="' . trans('label.detail') . '" data-title="' . trans('form.detail', ['menu' => $this->menu]) . '" data-icon="fa fa-search fa-fw" data-background="modal-success">' . trans('icon.detail') . '</a>';
//                }
                if (check_access('update', $this->slug)) {
                    $action .= '<a data-href="' . route($this->route . '.edit', encodeids($data->id_submission)) . '" class="btn btn-xs btn-warning btn-modal-form" title="' . trans('label.move') . '" data-title="' . trans('form.edit', ['menu' => $this->menu]) . '" data-icon="fa fa-search fa-fw" data-background="modal-warning">Submission ' . trans('icon.arrow') . ' Survey</a>';
                }
               /* if (check_access('delete', $this->slug)) {
                    $action .= '<a data-href="' . route($this->route . '.delete', encodeids($data->id_submission)) . '" class="btn btn-xs btn-danger btn-modal-action" title="' . trans('label.delete') . '" data-title="' . trans('form.delete', ['menu' => $this->menu]) . '" data-icon="fa fa-trash-o fa-fw" data-background="modal-danger">' . trans('icon.delete') . '</a>';
                }*/
                return $action;
            })
            ->make(true);
//        } catch (\Exception $e) {
//            abort(500);
//        }
    }


}