<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use Validator;

class MerchantController extends Controller
{
    private $bankModel;
    private $userModel;
    private $roleModel;
    private $addressModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu         = trans('menu.merchant.name');
        $this->route        = $this->routes['backend'].'merchant';
        $this->slug         = $this->slugs['backend'].'merchant';
        $this->view         = $this->views['backend'].'merchants';
        $this->breadcrumb   = '<li><a href="'.route($this->route.'.index').'">'.$this->menu.'</a></li>';
        # share parameters
        $this->share();
        # init model
        $this->model        = new \App\Models\Merchant;
        $this->bankModel        = new \App\Models\Bank;
        $this->userModel        = new \App\Models\User;
        $this->roleModel        = new \App\Models\Role;
        $this->addressModel        = new \App\Models\Address;
    }

    public function index()
    {
        try {
            $breadcrumb = $this->breadcrumbs($this->breadcrumb.'<li class="active">'.trans('label.view').'</li>');
            return view($this->view.'.index', compact('breadcrumb'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables()
    {
        try {
            $data = numrows($this->model->sql()->get());
            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    $action  = null;
                    if (check_access('detail', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.detail', encodeids($data->id)).'" class="btn btn-xs btn-success btn-modal-action" title="'.trans('label.detail').'" data-title="'.trans('form.detail', ['menu' => $this->menu]).'" data-icon="fa fa-search fa-fw" data-background="modal-primary">'.trans('icon.detail').'</a>';
                    }
                    if (check_access('update', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.edit', encodeids($data->id)).'" class="btn btn-xs btn-primary btn-modal-form" title="'.trans('label.edit').'" data-title="'.trans('form.edit', ['menu' => $this->menu]).'" data-icon="fa fa-edit fa-fw" data-background="modal-primary">'.trans('icon.edit').'</a>';
                    }
                    if (check_access('activate', $this->slug) && $data->active_id==0) {
                        $action .= '<a data-href="'.route($this->route.'.activate.get', encodeids($data->id)).'" class="btn btn-xs btn-success btn-modal-action" title="'.trans('label.activate').'" data-title="'.trans('form.activate', ['menu' => $this->menu]).'" data-icon="fa fa-check fa-fw" data-background="modal-success">'.trans('icon.activate').'</a>';
                    }
                    if (check_access('deactivate', $this->slug) && $data->active_id==1) {
                        $action .= '<a data-href="'.route($this->route.'.deactivate.get', encodeids($data->id)).'" class="btn btn-xs btn-danger btn-modal-action" title="'.trans('label.deactivate').'" data-title="'.trans('form.deactivate', ['menu' => $this->menu]).'" data-icon="fa fa-ban fa-fw" data-background="modal-danger">'.trans('icon.deactivate').'</a>';
                    }
                    if (check_access('delete', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.delete', encodeids($data->id)).'" class="btn btn-xs btn-danger btn-modal-action" title="'.trans('label.delete').'" data-title="'.trans('form.delete', ['menu' => $this->menu]).'" data-icon="fa fa-trash-o fa-fw" data-background="modal-danger">'.trans('icon.delete').'</a>';
                    }
                    return $action;
                })
                ->make(true);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function detail($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.detail', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        try {
            $banks = $this->bankModel->all();
            return view($this->view.'.form.create', compact('banks'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'phone' => 'required|max:255',
                'bank' => 'required',
                'address' => 'required|max:255',
                'email' => 'required|max:255|email|unique:'.$this->userModel->table.',email,NULL,id,deleted_at,NULL',
                'password' => 'required|min:8|confirmed|regex:'.regex_password_rules(),
            ], [
                'regex' => 'The :attribute format must be contained:'.regex_password_messages()
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route.'.create')
                    ->withErrors($validator)
                    ->withInput();
            }

            $role = $this->roleModel->where('slug', 'merchant')->first();

            $address = $this->addressModel->create([
                'address' => $request->address,
            ]);

            $user = $this->userModel->create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password)
            ])->user_role()->create([
                'role_id'   => $role->id
            ]);

            $data = $this->model;
            $data->user_id = $user->id;
            $data->address_id = $address->id;
            $data->bank_id = $request->bank;
            $data->phone = $request->phone;
            $data->password = bcrypt($request->password);
            $data->created_by = auth()->user()->id;
            $data->save();

            action_message('create', $this->menu);
            return json_encode(['redirect' => route($this->route.'.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function edit($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            $banks = $this->bankModel->all();
            return view($this->view.'.form.edit', compact('data', 'banks'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'bank' => 'required',
                'address' => 'required|max:255',
                'phone' => 'required|max:255',
                'email' => 'required|max:255|email|unique:'.$this->userModel->table.',email,'.$data->user_id.',id,deleted_at,NULL',
                'password' => 'min:8|confirmed|regex:'.regex_password_rules(),
            ], [
                'regex' => 'The :attribute format must be contained:'.regex_password_messages()
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route.'.edit', encodeids($id))
                    ->withErrors($validator)
                    ->withInput();
            }

            $data->update([
                'phone' => $request->phone,
                'bank_id' => $request->bank
            ]);

            $user = $this->userModel->find($data->user_id);

            $user->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password)
            ]);

            $address = $this->addressModel->find($data->address_id);

            $address->update([
                'address'      => $request->address,
            ]);

            action_message('update', $this->menu);
            return json_encode(['redirect' => route($this->route.'.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function getActivate($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.activate', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function postActivate($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->active = 1;
            $data->update();
            action_message('activate', $this->menu);
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function getDeactivate($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.deactivate', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function postDeactivate($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->active = 0;
            $data->update();
            action_message('deactivate', $this->menu);
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function delete($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.delete', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function destroy($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->delete();
            action_message('delete', $this->menu);
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
