<?php

namespace App\Http\Controllers\Backend;

use App\Models\Status;
use App\Models\SurveyByField;
use App\Models\SurveyByPhone;
use App\Models\Surveyor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use Validator;

class SurveyController extends Controller
{

    private $surveyorModel;
    private $statusModel;
    private $provinceModel;
    private $cityModel;
    private $districtModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu = trans('menu.survey.name');
        $this->route = $this->routes['backend'] . 'survey';
        $this->slug = $this->slugs['backend'] . 'survey';
        $this->view = $this->views['backend'] . 'surveys';
        $this->breadcrumb = '<li><a href="' . route($this->route . '.index') . '">' . $this->menu . '</a></li>';
        # share parameters
        $this->share();
        # init model
        $this->model = new \App\Models\Survey;
        $this->surveyorModel = new \App\Models\Surveyor;
        $this->statusModel = new \App\Models\Status;
        $this->provinceModel = new \App\Models\Province;
        $this->cityModel = new \App\Models\City;
        $this->districtModel = new \App\Models\District;
    }

    public function index()
    {
        try {
            $breadcrumb = $this->breadcrumbs($this->breadcrumb . '<li class="active">' . trans('label.view') . '</li>');
            return view($this->view . '.index', compact('breadcrumb'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables()
    {
        try {
            $data = numrows($this->model->sql()->get());

            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    $action = null;
                    if (check_access('detail', $this->slug)) {
                        $action .= '<a data-href="' . route($this->route . '.detail', encodeids($data->id)) . '" class="btn btn-xs btn-success btn-modal-action" title="' . trans('label.detail') . '" data-title="' . trans('form.detail', ['menu' => $this->menu]) . '" data-icon="fa fa-search fa-fw" data-background="modal-success">' . trans('icon.detail') . '</a>';
                    }
                    if (check_access('update', $this->slug)) {
                        $action .= '<a data-href="' . route($this->route . '.edit', encodeids($data->id)) . '" class="btn btn-xs btn-warning btn-modal-form" title="' . trans('label.edit') . '" data-title="' . trans('form.edit', ['menu' => $this->menu]) . '" data-icon="fa fa-edit fa-fw" data-background="modal-warning">' . trans('icon.edit') . '</a>';
                    }
                    if (check_access('update', $this->slug)/*  && $data->status_id == 10*/) {
                        $action .= '<a href="'.route($this->routes['backend'].'compare.index', ['id' => encodeids($data->id)]).'" class="btn btn-xs btn-primary" title="'.trans('label.compare').'">'.trans('icon.compare').' compare</a>';
                    }
//                    if (check_access('delete', $this->slug)) {
//                        $action .= '<a data-href="' . route($this->route . '.delete', encodeids($data->id)) . '" class="btn btn-xs btn-danger btn-modal-action" title="' . trans('label.delete') . '" data-title="' . trans('form.delete', ['menu' => $this->menu]) . '" data-icon="fa fa-trash-o fa-fw" data-background="modal-danger">' . trans('icon.delete') . '</a>';
//                    }
                    return $action;
                })
                ->make(true);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function detail($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);

            $fieldModel = SurveyByField::where('id', $id)->value('status_id');
            $fieldStatus = Status::where('id', $fieldModel)->value('name');

            $phoneModel = SurveyByPhone::where('id', $id)->value('status_id');
            $phoneStatus = Status::where('id', $phoneModel)->value('name');
//            debug($fieldStatusField, true);
            return view($this->view . '.form.detail', compact('data', 'fieldStatus', 'phoneStatus'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        try {
            $surveyors = $this->surveyorModel->all();
            $status = $this->statusModel->all();
            return view($this->view . '.form.create', compact('surveyors'), compact('status'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_name' => 'required|max:255',
                'user_nik' => 'required|max:255',
                'user_employment' => 'required|max:255',
                'user_phone_number' => 'required|max:255',
                'user_work_period' => 'required|max:255',
                'user_placement_location' => 'required|max:255',
                'company_hrd_name' => 'required|max:255',
                'company_industry' => 'required|max:255',
                'company_call_number' => 'required|max:255',
                'company_bank_payroll' => 'required|max:255',
                'interviewee_name' => 'required|max:255',
                'interviewee_employment' => 'required|max:255',
                'surveyor' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route . '.create')
                    ->withErrors($validator)
                    ->withInput();
            }


            $data = $this->model;
            $data->user_name = $request->user_name;
            $data->user_nik = $request->user_nik;
            $data->user_employment = $request->user_employment;
            $data->user_phone_number = $request->user_phone_number;
            $data->user_work_period = $request->user_work_period;
            $data->user_placement_location = $request->user_placement_location;
            $data->company_hrd_name = $request->company_hrd_name;
            $data->company_industry = $request->company_industry;
            $data->company_call_number = $request->company_call_number;
            $data->company_bank_payroll = $request->company_bank_payroll;
            $data->interviewee_name = $request->interviewee_name;
            $data->interviewee_employment = $request->interviewee_employment;
            $data->status_name = $request->status_name;
            $data->surveyor_id = $request->surveyor;
            $data->status_id = $request->status;
            $data->save();

            action_message('create', $this->menu);
            return json_encode(['redirect' => route($this->route . '.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function edit($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $status = $this->statusModel->all();

            $provinces = $this->provinceModel->all();
            $cities = $this->cityModel->all();
            $districts = $this->districtModel->all();

            $fieldModel = SurveyByField::where('id', $id)->value('status_id');
            $fieldModelSurveyor = SurveyByField::where('id', $id)->value('surveyor_id');
            $fieldStatus = Status::where('id', $fieldModel)->value('name');
            $surveyors = Surveyor::where('id', $fieldModelSurveyor)->value('name');

            $phoneModel = SurveyByPhone::where('id', $id)->value('status_id');
            $phoneStatus = Status::where('id', $phoneModel)->value('name');

            return view($this->view . '.form.edit', compact('data', 'surveyors', 'status',
                'provinces', 'cities', 'districts', 'fieldStatus', 'phoneStatus'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $surveyorModel = Surveyor::where('id', $request->surveyor)->value('user_id');

            $validator = Validator::make($request->all(), [
                'surveyor' => 'required|max:255',
                'status' => 'required|max:255',

            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route . '.edit', encodeids($id))
                    ->withErrors($validator)
                    ->withInput();
            }

//            debug($surveyorModel, true);
            $data->user_id = $surveyorModel;
            $data->surveyor_id = $request->surveyor;
            $data->status_id = $request->status;

            $data->update();

            action_message('update', $this->menu);
            return json_encode(['redirect' => route($this->route . '.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function delete($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view . '.form.delete', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function destroy($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->delete();
            action_message('delete', $this->menu);
            return redirect()->route($this->route . '.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
