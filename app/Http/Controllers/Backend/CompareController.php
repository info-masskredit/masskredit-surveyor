<?php
/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 29/07/18
 * Time: 19.01
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @property \App\Models\Survey model
 */
class CompareController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->menu         = trans('menu.compare.name');
        $this->route        = $this->routes['backend'].'compare';
        $this->route2        = $this->routes['backend'].'survey';
        $this->slug         = $this->slugs['backend'].'compare';
        $this->view         = $this->views['backend'].'compares';
        $this->breadcrumb   = '<li><a href="'.route($this->route.'.index').'">'.$this->menu.'</a></li>';
        view()->share([
            'menu'          => $this->menu,
            'route'         => $this->route,
            'slug'          => $this->slug,
            'view'          => $this->view,
        ]);
        # model
        $this->model        = new \App\Models\Survey;
        $this->phone        = new \App\Models\SurveyByPhone;
        $this->field        = new \App\Models\SurveyByField;

    }

    public function index($id = null)
    {
        try {
            $id = (!empty($id)) ? decodeids($id) : auth()->user()->id ;
            $breadcrumb = $this->breadcrumbs($this->breadcrumb.'<li class="active">'.trans('label.view').'</li>');
            $survey = $this->model->findOrFail($id);
            $byphone = $this->phone->findOrFail($id);
            $byfield = $this->field->findOrFail($id);
            return view($this->view.'.index', compact('breadcrumb', 'survey', 'byfield', 'byphone'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function updatephone(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);



            $data->user_name = $request->user_name;
            $data->user_nik = $request->user_nik;
            $data->user_employment = $request->user_employment;
            $data->user_phone_number = $request->user_phone_number;
            $data->user_work_period = $request->user_work_period;
            $data->user_placement_location = $request->user_placement_location;
            $data->company_hrd_name = $request->company_hrd_name;
            $data->company_industry = $request->company_industry;
            $data->company_call_number = $request->company_call_number;
            $data->company_bank_payroll = $request->company_bank_payroll;
            $data->interviewee_name = $request->interviewee_name;
            $data->interviewee_employment = $request->interviewee_employment;
            $data->work_location = $request->work_location;
            $data->econ_address = $request->econ_address;
            $data->econ_name = $request->econ_name;
            $data->econ_phone = $request->econ_phone;
            $data->status_id = 13;


            $data->update();

            action_message('compare', 'phone');
            return redirect()->route($this->route2.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function updatefield(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);


            $data->user_name = $request->user_name;
            $data->user_nik = $request->user_nik;
            $data->user_employment = $request->user_employment;
            $data->user_phone_number = $request->user_phone_number;
            $data->user_work_period = $request->user_work_period;
            $data->user_placement_location = $request->user_placement_location;
            $data->company_hrd_name = $request->company_hrd_name;
            $data->company_industry = $request->company_industry;
            $data->company_call_number = $request->company_call_number;
            $data->company_bank_payroll = $request->company_bank_payroll;
            $data->interviewee_name = $request->interviewee_name;
            $data->interviewee_employment = $request->interviewee_employment;
            $data->work_location = $request->work_location;
            $data->econ_address = $request->econ_address;
            $data->econ_name = $request->econ_name;
            $data->econ_phone = $request->econ_phone;

            $data->update();

            action_message('compare', 'field');
            return redirect()->route($this->route2.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }

}