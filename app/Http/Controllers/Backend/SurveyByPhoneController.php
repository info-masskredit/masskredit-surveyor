<?php

namespace App\Http\Controllers\Backend;

use App\Models\Surveyor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use Validator;

/**
 * @property \App\Models\SurveyByPhone model
 */
class SurveyByPhoneController extends Controller
{

    private $surveyorModel;
    private $statusModel;
    private $provinceModel;
    private $cityModel;
    private $districtModel;

    public function __construct()
    {
        parent::__construct();
        $this->menu         = trans('menu.survey_phone.name');
        $this->route        = $this->routes['backend'].'survey_phone';
        $this->slug         = $this->slugs['backend'].'survey_phone';
        $this->view         = $this->views['backend'].'survey_phone';
        $this->breadcrumb   = '<li><a href="'.route($this->route.'.index').'">'.$this->menu.'</a></li>';
        # share parameters
        $this->share();
        # init model
        $this->model        = new \App\Models\SurveyByPhone;
        $this->surveyorModel = new \App\Models\Surveyor;
        $this->statusModel = new \App\Models\Status;
        $this->provinceModel = new \App\Models\Province;
        $this->cityModel = new \App\Models\City;
        $this->districtModel = new \App\Models\District;
    }

    public function index()
    {
        try {
            $breadcrumb = $this->breadcrumbs($this->breadcrumb.'<li class="active">'.trans('label.view').'</li>');
            return view($this->view.'.index', compact('breadcrumb'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function datatables()
    {
        try {
            $data = numrows($this->model->sql()->get());
            return Datatables::of($data)
                ->addColumn('action', function ($data) {
                    $action  = null;
                    if (check_access('detail', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.detail', encodeids($data->id)).'" class="btn btn-xs btn-success btn-modal-action" title="'.trans('label.detail').'" data-title="'.trans('form.detail', ['menu' => $this->menu]).'" data-icon="fa fa-search fa-fw" data-background="modal-success">'.trans('icon.detail').'</a>';
                    }
                    if (check_access('update', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.edit', encodeids($data->id)).'" class="btn btn-xs btn-warning btn-modal-form" title="'.trans('label.edit').'" data-title="'.trans('form.edit', ['menu' => $this->menu]).'" data-icon="fa fa-edit fa-fw" data-background="modal-warning">'.trans('icon.edit').'</a>';
                    }
                   /* if (check_access('delete', $this->slug)) {
                        $action .= '<a data-href="'.route($this->route.'.delete', encodeids($data->id)).'" class="btn btn-xs btn-danger btn-modal-action" title="'.trans('label.delete').'" data-title="'.trans('form.delete', ['menu' => $this->menu]).'" data-icon="fa fa-trash-o fa-fw" data-background="modal-danger">'.trans('icon.delete').'</a>';
                    }*/
                    return $action;
                })
                ->make(true);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function detail($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.detail', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        try {
            $surveyors = $this->surveyorModel->all();
            $status = $this->statusModel->all();
            return view($this->view.'.form.create', compact('surveyors'), compact('status'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_name' => 'required|max:255',
                'user_nik' => 'required|max:255',
                'user_employment' => 'required|max:255',
                'user_phone_number' => 'required|max:255',
                'user_work_period' => 'required|max:255',
                'user_placement_location' => 'required|max:255',
                'company_hrd_name' => 'required|max:255',
                'company_industry' => 'required|max:255',
                'company_call_number' => 'required|max:255',
                'company_bank_payroll' => 'required|max:255',
                'interviewee_name' => 'required|max:255',
                'interviewee_employment' => 'required|max:255',
                'surveyor' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->route($this->route.'.create')
                    ->withErrors($validator)
                    ->withInput();
            }


            $data = $this->model;
            $data->user_name = $request->user_name;
            $data->user_nik = $request->user_nik;
            $data->user_employment = $request->user_employment;
            $data->user_phone_number = $request->user_phone_number;
            $data->user_work_period = $request->user_work_period;
            $data->user_placement_location = $request->user_placement_location;
            $data->company_hrd_name = $request->company_hrd_name;
            $data->company_industry = $request->company_industry;
            $data->company_call_number = $request->company_call_number;
            $data->company_bank_payroll = $request->company_bank_payroll;
            $data->interviewee_name = $request->interviewee_name;
            $data->interviewee_employment = $request->interviewee_employment;
            $data->status_name = $request->status_name;
            $data->surveyor_id = $request->surveyor;
            $data->status_id = $request->status;
            $data->save();

            action_message('create', $this->menu);
            return json_encode(['redirect' => route($this->route.'.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function edit($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $status = $this->statusModel->all();
            $surveyors = $this->surveyorModel->all();
            $provinces = $this->provinceModel->all();
            $cities = $this->cityModel->all();
            $districts = $this->districtModel->all();
            return view($this->view . '.form.edit', compact('data', 'surveyors', 'status',
                'provinces', 'cities', 'districts'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);

            $surveyorModel = Surveyor::where('id', $request->surveyor)->value('user_id');

            $data->user_name = $request->user_name;
            $data->user_nik = $request->user_nik;
            $data->user_employment = $request->user_employment;
            $data->user_phone_number = $request->user_phone_number;
            $data->user_work_period = $request->user_work_period;
            $data->user_placement_location = $request->user_placement_location;
            $data->company_hrd_name = $request->company_hrd_name;
            $data->company_industry = $request->company_industry;
            $data->company_call_number = $request->company_call_number;
            $data->company_bank_payroll = $request->company_bank_payroll;
            $data->interviewee_name = $request->interviewee_name;
            $data->interviewee_employment = $request->interviewee_employment;
            $data->status_name = $request->status_name;
            $data->surveyor_id = $request->surveyor;
            $data->status_id = $request->status;
            $data->province_id = $request->province;
            $data->city_id = $request->city;
            $data->district_id = $request->district;

            $data->user_id = $surveyorModel;

            $data->update();

            action_message('update', $this->menu);
            return json_encode(['redirect' => route($this->route . '.index')]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function delete($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->sql()->findOrFail($id);
            return view($this->view.'.form.delete', compact('data'));
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function destroy($id)
    {
        try {
            $id = decodeids($id);
            $data = $this->model->findOrFail($id);
            $data->delete();
            action_message('delete', $this->menu);
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
