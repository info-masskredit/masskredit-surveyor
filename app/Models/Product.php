<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Model as ModelContracts;

class Product extends Model implements ModelContracts
{
    use SoftDeletes;

    public $table = 'products';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['name', 'description', 'image'];

    public function sql()
    {
        return $this
            ->select(
                $this->table.'.id',
                $this->table.'.name',
                $this->table.'.image',
                $this->table.'.description'
            )->orderBy(
                $this->table.'.name'
            );
    }
}
