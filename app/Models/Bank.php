<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\Model as ModelContracts;

class Bank extends Model implements ModelContracts
{
    public $table = 'banks';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['code', 'name'];

    public function sql()
    {
        return $this
            ->select(
                $this->table.'.id',
                $this->table.'.name',
                $this->table.'.code'
            )->orderBy(
                $this->table.'.name'
            );

    }
}
