<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\Model as ModelContracts;

class Address extends Model implements ModelContracts
{
    public $table = 'addresses';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'province_id', 'city_id', 'district_id',
        'sub_district_id', 'rt_rw', 'address',
        'phone', 'zip', 'longitude', 'latitude'
    ];

    public function sql()
    {
        //
    }

    public function merchants()
    {
        return $this->hasMany(Merchant::class, 'merchant_id');
    }
}
