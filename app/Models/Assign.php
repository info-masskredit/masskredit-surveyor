<?php
/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 26/07/18
 * Time: 14.00
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Contracts\Model as ModelContracts;

class Assign extends Model implements ModelContracts
{
    public $table = 'assigns';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['survey_id', 'surveyor_id'];

    /**
     * Declare sql method for custom query.
     *
     * @return string
     */
    public function sql()
    {
        return $this
            ->select(
                $this->table.'.id',
                $this->table.'.survey_id',
                $this->table.'.surveyor_id'
            )->orderBy(
                $this->table.'.survey_id'
            );
    }
}