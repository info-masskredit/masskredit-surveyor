<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use App\Contracts\Model as ModelContracts;

class Image extends Model implements ModelContracts
{
    public $table = 'images';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['image'];

    protected $appends = ['image_url'];

    protected static function boot()
    {
        static::deleting(function ($model) {
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public') . DIRECTORY_SEPARATOR . $model->image;
            if (File::exists($path)) {
                File::delete($path);
            }
        });
    }

    public function getImageUrlAttribute()
    {
        return env('APP_URL') . Storage::url($this->attributes['image']);
    }

    public function sql()
    {
        //
    }

    public function users()
    {
        return $this->hasMany(User::class, 'avatar');
    }
}
