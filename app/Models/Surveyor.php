<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Model as ModelContracts;
use Illuminate\Support\Facades\DB;

class Surveyor extends Model implements ModelContracts
{
    use SoftDeletes;

    public $table = 'surveyors';

    private $provinceModel;
    private $cityModel;
    private $districtModel;
    private $surveyModel;
    private $assignModel;

    protected $dates = ['created_at', 'updated_at', 'deleted_at',  'login_date', 'expire_at'];

    protected $hidden = ['password', 'is_admin', 'remember_token', 'fcm_token'];

    protected $fillable = [
        'name',
        'phone_number',
        'nik',
        'email',
        'password',
        'province_id',
        'city_id',
        'district_id',
        'latitude',
        'longitude',
        'assigned_survey',
        'max_survey',
        'access_token',
        'surveyed_today',
        'unsurveyed_today',
        'total_survey_today',
        'fcm_token',
        'user_id'
    ];

    public function sql()
    {
        $this->provinceModel = new Province;
        $this->cityModel = new City;
        $this->districtModel = new District;

        return $this
            ->leftJoin($this->provinceModel->table, $this->provinceModel->table.'.id', '=', $this->table.'.province_id')
            ->leftJoin($this->cityModel->table, $this->cityModel->table.'.id', '=', $this->table.'.city_id')
            ->leftJoin($this->districtModel->table, $this->districtModel->table.'.id', '=', $this->table.'.district_id')
            ->select(
                $this->table.'.id',
                $this->table.'.name',
                $this->table.'.phone_number',
                $this->table.'.nik',
                $this->table.'.email',
                $this->table.'.latitude',
                $this->table.'.longitude',
                $this->table.'.assigned_survey',
                $this->table.'.max_survey',
                $this->provinceModel->table.'.name as provinces',
                $this->cityModel->table.'.name as cities',
                $this->districtModel->table.'.name as districts'

            )->orderBy(
                $this->table.'.name'
            );
    }

    public function sql2()
    {
        $this->provinceModel = new Province;
        $this->cityModel = new City;
        $this->districtModel = new District;
        $this->surveyModel = new Survey;
        $this->assignModel = new Assign;

        return $this
            ->leftJoin($this->provinceModel->table, $this->provinceModel->table.'.id', '=', $this->table.'.province_id')
            ->leftJoin($this->cityModel->table, $this->cityModel->table.'.id', '=', $this->table.'.city_id')
            ->leftJoin($this->districtModel->table, $this->districtModel->table.'.id', '=', $this->table.'.district_id')
            ->leftJoin($this->assignModel->table, $this->assignModel->table.'.surveyor_id', '=', $this->table.'.id')
            ->leftJoin($this->surveyModel->table, $this->surveyModel->table.'.id', '=', $this->assignModel.'.survey_id')
            ->select(
                $this->table.'.id',
                $this->table.'.name',
                $this->table.'.phone_number',
                $this->table.'.nik',
                $this->table.'.email',
                $this->provinceModel->table.'.name as provinces',
                $this->cityModel->table.'.name as cities',
                $this->districtModel->table.'.name as districts',
                $this->surveyModel->table.'.name'

            )->orderBy(
                $this->table.'.name'
            );
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
