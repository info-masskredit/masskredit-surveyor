<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Model as ModelContracts;

class District extends Model implements ModelContracts
{
    use SoftDeletes;

    private $cityModel;

    public $table = 'districts';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['name', 'city_id'];
  public function sql()
    {
        $this->cityModel = new City;

        return $this
            ->leftJoin($this->cityModel->table, $this->cityModel->table.'.id', '=', $this->table.'.city_id')
            ->select(
                $this->table.'.id',
                $this->table.'.name',
                $this->cityModel->table.'.name as city'
            )->orderBy(
                $this->table.'.name'
            );
    }
}
