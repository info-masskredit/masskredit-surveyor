<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Model as ModelContracts;

class Merchant extends Model implements ModelContracts
{
    use SoftDeletes;

    public $table = 'merchants';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['id', 'user_id', 'bank_id', 'address_id', 'phone', 'password', 'active'];

    protected $hidden = ['password'];

    public function sql()
    {
        $userModel = new User;
        $addressModel = new Address;
        $bankModel = new Bank;

        return $this
            ->leftJoin($userModel->table, $this->table . '.user_id', '=', $userModel->table . '.id')
            ->leftJoin($addressModel->table, $this->table . '.address_id', '=', $addressModel->table . '.id')
            ->leftJoin($bankModel->table, $this->table . '.bank_id', '=', $addressModel->table . '.id')
            ->select(
                $this->table . '.id',
                $userModel->table . '.name as name',
                $userModel->table . '.email as email',
                $addressModel->table . '.address as address',
                $this->table . '.bank_id',
                $this->table . '.phone',
                $this->table . '.password',
                $this->table . '.active AS active_id',
                \DB::raw('to_char(' . $this->table . '.created_at, \'dd-mm-YYYY HH24:MI\') AS join_date'),
                \DB::raw('CASE ' . $this->table . '.active WHEN TRUE THEN \'Active\' ELSE \'Not Active\' END AS active')
            )->orderBy(
                $userModel->table . '.name'
            );
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
