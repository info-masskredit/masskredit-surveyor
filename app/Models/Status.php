<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Contracts\Model as ModelContracts;

class Status extends Model implements ModelContracts
{
    public $table = 'status';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['name'];

    public static $fieldStatusForMobile = [3, 6, 9];

    public function sql()
    {
        return $this
            ->select(
                $this->table.'.id',
                $this->table.'.name'
            )->orderBy(
                $this->table.'.name'
            );

    }

    public function scopeAdmin(Builder $query)
    {
        return $query->whereIn('id', [4, 8]);
    }

    public function scopePhone(Builder $query)
    {
        return $query->whereIn('id', [1, 2, 5, 6]);
    }

    public function scopeField(Builder $query)
    {
        return $query->whereIn('id', [1, 2, 6, 7, 9]);
    }
}
