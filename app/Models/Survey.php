<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Model as ModelContracts;
use Illuminate\Support\Facades\DB;

class Survey extends Model implements ModelContracts
{
    use SoftDeletes;

    private $surveyorModel;
    private $provinceModel;
    private $cityModel;
    private $districtModel;
    private $fieldModel;
    private $statusModel;
    private $statusField;

    public $table = 'surveys';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'user_name',
        'user_nik',
        'user_employment',
        'user_phone_number',
        'user_work_period',
        'user_placement_location',
        'company_hrd_name',
        'company_industry',
        'company_call_number',
        'company_bank_payroll',
        'interviewee_name',
        'interviewee_employment',
        'status_name',
        'surveyor_id',
        'status_id',
        'province_id',
        'city_id',
        'district_id',
        'econ_relationship',
        'position',
        'proof_of_salary',
        'work_location_lat',
        'work_location_lon',
        'work_location',
        'photo_location',
        'photo_interviewee',
        'survey_empty_reason',
        'user_id',
        'is_offline'
    ];


    public function sql()
    {
        $this->surveyorModel = new Surveyor;
        $this->statusModel = new Status;
        $this->provinceModel = new Province;
        $this->cityModel = new City;
        $this->districtModel = new District;
        $this->fieldModel = new SurveyByField;
//        $statusField = DB::table('status as stat');
//        $field = SurveyByField::where('id', $this->table.'.id')->first();

        return $this
            ->leftJoin($this->surveyorModel->table, $this->surveyorModel->table . '.id', '=', $this->table . '.surveyor_id')
            ->leftJoin($this->provinceModel->table, $this->provinceModel->table . '.id', '=', $this->table . '.province_id')
            ->leftJoin($this->cityModel->table, $this->cityModel->table . '.id', '=', $this->table . '.city_id')
            ->leftJoin($this->districtModel->table, $this->districtModel->table . '.id', '=', $this->table . '.district_id')
            ->leftJoin($this->fieldModel->table, $this->fieldModel->table . '.id', '=', $this->table . '.id')
            ->leftJoin($this->statusModel->table , $this->statusModel->table . '.id', '=', $this->table . '.status_id')
            ->select(
                $this->table . '.id',
                $this->table . '.user_name',
                $this->table . '.user_nik',
                $this->table . '.user_employment',
                $this->table . '.user_phone_number',
                $this->table . '.user_work_period',
                $this->table . '.user_placement_location',
                $this->table . '.company_hrd_name',
                $this->table . '.company_industry',
                $this->table . '.company_call_number',
                $this->table . '.company_bank_payroll',
                $this->table . '.interviewee_name',
                $this->table . '.interviewee_employment',
                $this->table . '.status_name',
                $this->table . '.status_id',
                $this->table . '.work_location',
                $this->table . '.econ_relationship',
                $this->table . '.position',
                $this->table . '.proof_of_salary',
                $this->table . '.user_id',
                $this->table . '.econ_name',
                $this->table . '.econ_phone',
                $this->table . '.econ_address',
                $this->table . '.is_offline',
                $this->surveyorModel->table . '.name as surveyor',
//                $this->statusField->table . '.name as status_2',
                $this->statusModel->table . '.name as status',
                $this->provinceModel->table . '.name as province',
                $this->cityModel->table . '.name as city',
                $this->districtModel->table . '.name as district'
            )->orderBy(
                $this->table . '.created_at'
            );
    }

    public function surveylist()
    {
        $this->surveyorModel = new Surveyor;
        $this->statusModel = new Status;
        $this->provinceModel = new Province;
        $this->cityModel = new City;
        $this->districtModel = new District;

        return $this
            ->leftJoin($this->surveyorModel->table, $this->surveyorModel->table . '.id', '=', $this->table . '.surveyor_id')
            ->leftJoin($this->statusModel->table, $this->statusModel->table . '.id', '=', $this->table . '.status_id')
            ->leftJoin($this->provinceModel->table, $this->provinceModel->table . '.id', '=', $this->table . '.province_id')
            ->leftJoin($this->cityModel->table, $this->cityModel->table . '.id', '=', $this->table . '.city_id')
            ->leftJoin($this->districtModel->table, $this->districtModel->table . '.id', '=', $this->table . '.district_id')
            ->select(
                $this->table . '.id',
                $this->table . '.user_name',
                $this->table . '.user_nik',
                $this->table . '.user_employment',
                $this->table . '.user_phone_number',
                $this->table . '.user_work_period',
                $this->table . '.user_placement_location',
                $this->table . '.company_hrd_name',
                $this->table . '.company_industry',
                $this->table . '.company_call_number',
                $this->table . '.company_bank_payroll',
                $this->table . '.interviewee_name',
                $this->table . '.interviewee_employment',
                $this->table . '.status_name',
                $this->surveyorModel->table . '.name as surveyor',
                $this->statusModel->table . '.name as status',
                $this->provinceModel->table . '.name as province',
                $this->cityModel->table . '.name as city',
                $this->districtModel->table . '.name as district'
            )->orderBy(
                $this->table . '.created_at'
            );
    }

    public function assign()
    {
        return $this->hasMany(Assign::class, 'survey_id', 'survey_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
