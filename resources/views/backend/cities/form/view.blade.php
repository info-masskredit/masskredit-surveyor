<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.province') !!}</label>
            {!! Form::viewText($data->province) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.name') !!}</label>
            {!! Form::viewText($data->name) !!}
        </div>
    </div>
</div>
