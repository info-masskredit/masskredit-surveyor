{!! Form::open(['route' => $route.'.store', 'method' => 'POST', 'files' => true]) !!}
<div class="box-body">
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_name').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_name" value="{!! old('user_name') !!}" placeholder="{!! trans('label.user_name') !!}">
            @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_nik') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_nik').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_nik" value="{!! old('user_nik') !!}" placeholder="{!! trans('label.user_nik') !!}">
            @if ($errors->has('user_nik'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_nik') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_employment') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_employment').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_employment" value="{!! old('user_employment') !!}" placeholder="{!! trans('label.user_employment') !!}">
            @if ($errors->has('user_employment'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_employment') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_phone_number') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_phone_number').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_phone_number" value="{!! old('user_phone_number') !!}" placeholder="{!! trans('label.user_phone_number') !!}">
            @if ($errors->has('user_phone_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_phone_number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_work_period') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_work_period').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_work_period" value="{!! old('user_work_period') !!}" placeholder="{!! trans('label.user_work_period') !!}">
            @if ($errors->has('user_work_period'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_work_period') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_placement_location') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_placement_location').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_placement_location" value="{!! old('user_placement_location') !!}" placeholder="{!! trans('label.user_placement_location') !!}">
            @if ($errors->has('user_placement_location'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_placement_location') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('company_hrd_name') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.company_hrd_name').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="company_hrd_name" value="{!! old('company_hrd_name') !!}" placeholder="{!! trans('label.company_hrd_name') !!}">
            @if ($errors->has('company_hrd_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('company_hrd_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('company_industry') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.company_industry').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="company_industry" value="{!! old('company_industry') !!}" placeholder="{!! trans('label.company_industry') !!}">
            @if ($errors->has('company_industry'))
                <span class="help-block">
                    <strong>{{ $errors->first('company_industry') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('company_call_number') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.company_call_number').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="company_call_number" value="{!! old('company_call_number') !!}" placeholder="{!! trans('label.company_call_number') !!}">
            @if ($errors->has('company_call_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('company_call_number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('company_bank_payroll') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.company_bank_payroll').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="company_bank_payroll" value="{!! old('company_bank_payroll') !!}" placeholder="{!! trans('label.company_bank_payroll') !!}">
            @if ($errors->has('company_bank_payroll'))
                <span class="help-block">
                    <strong>{{ $errors->first('company_bank_payroll') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('interviewee_name') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.interviewee_name').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="interviewee_name" value="{!! old('interviewee_name') !!}" placeholder="{!! trans('label.interviewee_name') !!}">
            @if ($errors->has('interviewee_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('interviewee_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('interviewee_employment') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.interviewee_employment').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="interviewee_employment" value="{!! old('interviewee_employment') !!}" placeholder="{!! trans('label.interviewee_employment') !!}">
            @if ($errors->has('interviewee_employment'))
                <span class="help-block">
                    <strong>{{ $errors->first('interviewee_employment') }}</strong>
                </span>
            @endif
        </div>
    </div>
    {{--<div class="col-md-6">
        <div class="form-group{{ $errors->has('status_name') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.status_name').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="status_name" value="{!! old('status_name') !!}" placeholder="{!! trans('label.status_name') !!}">
            @if ($errors->has('status_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('status_name') }}</strong>
                </span>
            @endif
        </div>
    </div>--}}
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('surveyor') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.surveyor').trans('icon.mandatory') !!}</label>
            <select class="form-control select2" name="surveyor" data-placeholder="{!! trans('label.surveyor') !!}" autofocus>
                <option></option>
                @foreach ($surveyors as $surveyor)
                    <option value="{!! $surveyor->id !!}"{!! old('surveyor')==$surveyor->id ? ' selected' : '' !!}>{!! $surveyor->name !!}</option>
                @endforeach
            </select>
            @if ($errors->has('surveyor'))
                <span class="help-block">
                <strong>{!! $errors->first('surveyor') !!}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.status').trans('icon.mandatory') !!}</label>
            <select class="form-control select2" name="status" data-placeholder="{!! trans('label.status') !!}" autofocus>
                <option></option>
                @foreach ($status as $status)
                    <option value="{!! $status->id !!}"{!! old('status')==$status->id ? ' selected' : '' !!}>{!! $status->name !!}</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="help-block">
                <strong>{!! $errors->first('status') !!}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-primary">{!! trans('button.save') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
