{!! Form::open(['route' => $route.'.store', 'method' => 'POST']) !!}
<div class="box-body">
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.name').trans('icon.mandatory') !!}</label>
                <input type="text" class="form-control" name="name" value="{!! old('name') !!}" placeholder="{!! trans('label.name') !!}">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{!! $errors->first('name') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.phone_number').trans('icon.mandatory') !!}</label>
                <input type="phone_number" class="form-control" name="phone_number" value="{!! old('phone_number') !!}" placeholder="{!! trans('label.phone_number') !!}">
                @if ($errors->has('phone_number'))
                    <span class="help-block">
                        <strong>{!! $errors->first('phone_number') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.email').trans('icon.mandatory') !!}</label>
                <input type="email" class="form-control" name="email" value="{!! old('email') !!}" placeholder="{!! trans('label.email') !!}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{!! $errors->first('email') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('nik') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.nik').trans('icon.mandatory') !!}</label>
                <input type="nik" class="form-control" name="nik" value="{!! old('nik') !!}" placeholder="{!! trans('label.nik') !!}">
                @if ($errors->has('nik'))
                    <span class="help-block">
                        <strong>{!! $errors->first('nik') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.province').trans('icon.mandatory') !!}</label>
                <select class="form-control select2" name="province" data-placeholder="{!! trans('label.province') !!}" autofocus>
                    <option></option>
                    @foreach ($provinces as $province)
                        <option value="{!! $province->id !!}"{!! old('province')==$province->id ? ' selected' : '' !!}>{!! $province->name !!}</option>
                    @endforeach
                </select>
                @if ($errors->has('province'))
                    <span class="help-block">
                <strong>{!! $errors->first('province') !!}</strong>
            </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.city').trans('icon.mandatory') !!}</label>
                <select class="form-control select2" name="city" data-placeholder="{!! trans('label.city') !!}" autofocus>
                    <option></option>
                    @foreach ($cities as $city)
                        <option value="{!! $city->id !!}"{!! old('city')==$city->id ? ' selected' : '' !!}>{!! $city->name !!}</option>
                    @endforeach
                </select>
                @if ($errors->has('city'))
                    <span class="help-block">
                <strong>{!! $errors->first('city') !!}</strong>
            </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.district').trans('icon.mandatory') !!}</label>
                <select class="form-control select2" name="district" data-placeholder="{!! trans('label.district') !!}" autofocus>
                    <option></option>
                    @foreach ($districts as $district)
                        <option value="{!! $district->id !!}"{!! old('district')==$district->id ? ' selected' : '' !!}>{!! $district->name !!}</option>
                    @endforeach
                </select>
                @if ($errors->has('district'))
                    <span class="help-block">
                <strong>{!! $errors->first('district') !!}</strong>
            </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('max_survey') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.max_survey').trans('icon.mandatory') !!}</label>
                <input type="number" max="10" class="form-control" name="max_survey" value="{!! old('max_survey') !!}" placeholder="{!! trans('label.max_survey') !!}">
                @if ($errors->has('max_survey'))
                    <span class="help-block">
                        <strong>{!! $errors->first('max_survey') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.password').trans('icon.mandatory') !!}</label>
                <input type="password" class="form-control" name="password" placeholder="{!! trans('label.password') !!}">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{!! $errors->first('password') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.password_confirmation').trans('icon.mandatory') !!}</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="{!! trans('label.password_confirmation') !!}">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{!! $errors->first('password_confirmation') !!}</strong>
                    </span>
                @endif
            </div>
        </div>

    </div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-warning">{!! trans('button.save') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
