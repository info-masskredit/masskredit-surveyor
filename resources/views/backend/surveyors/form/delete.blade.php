{!! Form::open(['route' => [$route.'.destroy', encodeids($data->id)], 'method' => 'DELETE']) !!}
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.name') !!}</label>
                {!! Form::viewText($data->name) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.phone_number') !!}</label>
                {!! Form::viewText($data->phone_number) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.nik') !!}</label>
                {!! Form::viewText($data->nik) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.email') !!}</label>
                {!! Form::viewText($data->email) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.province') !!}</label>
                {!! Form::viewText($data->provinces) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.city') !!}</label>
                {!! Form::viewText($data->cities) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.district') !!}</label>
                {!! Form::viewText($data->districts) !!}
            </div>
        </div>
    </div>

</div>
<div class="box-footer">
    <button type="submit" class="btn btn-danger">{!! trans('button.delete') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
