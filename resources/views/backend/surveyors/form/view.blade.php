<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.name') !!}</label>
            {!! Form::viewText($data->name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.phone_number') !!}</label>
            {!! Form::viewText($data->phone_number) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.nik') !!}</label>
            {!! Form::viewText($data->nik) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.email') !!}</label>
            {!! Form::viewText($data->email) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.province') !!}</label>
            {!! Form::viewText($data->provinces) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.city') !!}</label>
            {!! Form::viewText($data->cities) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.district') !!}</label>
            {!! Form::viewText($data->districts) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.max_survey') !!}</label>
            {!! Form::viewText($data->max_survey) !!}
        </div>
    </div>
</div>

<hr>
<div class="modal-header modal-success">
    <h4 class="modal-title">Assignee Left</h4>
</div>
<div>
    <table class="table table-striped table-borderless table-tes" width="100%">
        <thead>
        <tr>
            <th>{!! trans('label.no') !!}</th>
            <th>{!! trans('label.name') !!}</th>
            <th>{!! trans('label.phone_number') !!}</th>
            <th>{!! trans('label.city') !!}</th>
            <th>{!! trans('label.district') !!}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
<hr>
<!DOCTYPE html>
<html>
<head>
    <style>

        #map {
            height: 400px;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="modal-header modal-success">
    <h4 class="modal-title">Last Known Location</h4>
</div>

<div id="map"></div>
<script>

    function initMap() {

        var uluru = {lat: {!! $data->latitude !!}, lng: {!! $data->longitude !!}};

        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 13, center: uluru});

        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSvLpDKs3x6Oo0xFoDhzL36lx5DtOyJQA&callback=initMap">
</script>
</body>
</html>

<hr>

<script type="text/javascript">
    $(function(){
        $('.table-tes').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            paging: false,
            info: false,
            ajax: '{!! route($route.".datatables2", $data->id) !!}',
            columns: [
                {data: 'no', searchable: false, width: '5%', className: 'center'},
                {data: 'user_name'},
                {data: 'user_nik'},
                {data: 'city'},
                {data: 'district'}
            ],
            drawCallback: function(){
                INIT.tooltip();
            },
        });
    });
</script>
