{!! Form::open(['route' => [$route.'.update', encodeids($data->id)], 'method' => 'PUT']) !!}
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.name').trans('icon.mandatory') !!}</label>
                <input type="text" class="form-control" name="name" value="{!! old('name')!==null ? old('name') : $data->name !!}" placeholder="{!! trans('label.name_ph') !!}">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{!! $errors->first('name') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.email').trans('icon.mandatory') !!}</label>
                <input type="email" class="form-control" name="email" value="{!! old('email')!==null ? old('email') : $data->email !!}" placeholder="{!! trans('label.email_ph') !!}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{!! $errors->first('email') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.phone').trans('icon.mandatory') !!}</label>
                <input type="text" class="form-control" name="phone" value="{!! old('phone')!==null ? old('phone') : $data->phone !!}" placeholder="{!! trans('label.phone_ph') !!}">
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{!! $errors->first('phone') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.bank').trans('icon.mandatory') !!}</label>
                <select class="form-control select2" name="bank" data-placeholder="{!! trans('label.bank') !!}" autofocus>
                    <option></option>
                    @foreach ($banks as $bank)
                        <option value="{!! $bank->id !!}"{!! old('bank') ? (old('bank')==$bank->id ? ' selected' : '') : ($data->bank_id == $bank->id ? ' selected' : '') !!}>{!! $bank->name !!}</option>
                    @endforeach
                </select>
                @if ($errors->has('bank'))
                    <span class="help-block">
                    <strong>{!! $errors->first('bank') !!}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.address').trans('icon.mandatory') !!}</label>
                <textarea class="form-control" rows="5" name="address">{!! old('address')!==null ? old('address') : $data->address !!}</textarea>
                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{!! $errors->first('address') !!}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.password').trans('icon.mandatory') !!}</label>
                <input type="password" class="form-control" name="password" placeholder="{!! trans('label.password_ph') !!}">
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{!! $errors->first('password') !!}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.password_confirmation').trans('icon.mandatory') !!}</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="{!! trans('label.password_confirmation_ph') !!}">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                    <strong>{!! $errors->first('password_confirmation') !!}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-primary">{!! trans('button.save') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
