<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.name') !!}</label>
            {!! Form::viewText($data->name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.email') !!}</label>
            {!! Form::viewText($data->email) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.phone') !!}</label>
            {!! Form::viewText($data->phone) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.address') !!}</label>
            {!! Form::viewText($data->address) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.bank') !!}</label>
            {!! Form::viewText($data->bank->name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.join_date') !!}</label>
            {!! Form::viewText($data->join_date) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.status') !!}</label>
            {!! Form::viewText($data->active) !!}
        </div>
    </div>
</div>
