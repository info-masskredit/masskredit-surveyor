<footer class="main-footer">
    <strong>Copyright &copy; {!! date('Y') !!} <a href="https://www.masskredit.co.id/" target="__blank">{{ trans('app.company') }}</a>.</strong> All rights reserved.
    <div class="pull-right hidden-xs">
        {!! trans('app.version') !!}
    </div>
</footer>
