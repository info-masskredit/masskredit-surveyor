<script type="text/javascript">
$(function(){
    $('.table-datatables').DataTable({
        processing: true,
        serverSide: true,
        columns: [
            {data: 'no', searchable: false, width: '5%', className: 'center'},
            {data: 'user_name'},
            {data: 'city_name'},
            {data: 'user_placement_location'},
            {data: 'is_offline'},
            {data: 'action', orderable: false, searchable: false, width: '15%', className: 'center action'}
        ],
        ajax: '{!! route($route.".datatables") !!}',
        drawCallback: function(){
            INIT.tooltip();
        },
    });
});
</script>
