{!! Form::open(['route' => $route.'.store', 'method' => 'POST', 'files' => true]) !!}
<div class="box-body">
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.user_name').trans('icon.mandatory') !!}</label>
            <input type="text" class="form-control" name="user_name" value="{!! old('user_name') !!}" placeholder="{!! trans('label.user_name') !!}">
            @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-primary">{!! trans('button.save') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
