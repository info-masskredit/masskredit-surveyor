{!! Form::open(['route' => $route.'.store', 'method' => 'POST', 'files' => true]) !!}
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_name') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_name" value="{!! old('user_name')!==null ? old('user_name') : $data->data->user_name !!}" placeholder="{!! trans('label.user_name') !!}">
                @if ($errors->has('user_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_nik') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_nik') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_nik" value="{!! old('user_nik')!==null ? old('user_nik') : $data->data->user_nik !!}" placeholder="{!! trans('label.user_nik') !!}">
                @if ($errors->has('user_nik'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_nik') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_employment') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_employment') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_employment" value="{!! old('user_employment')!==null ? old('user_employment') : $data->data->user_employment !!}" placeholder="{!! trans('label.user_employment') !!}">
                @if ($errors->has('user_employment'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_employment') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_phone_number') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_phone_number') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_phone_number" value="{!! old('user_phone_number')!==null ? old('user_phone_number') : $data->data->user_phone_number !!}" placeholder="{!! trans('label.user_phone_number') !!}">
                @if ($errors->has('user_phone_number'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_phone_number') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_work_period') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_work_period') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_work_period" value="{!! old('user_work_period')!==null ? old('user_work_period') : $data->data->user_work_period !!}" placeholder="{!! trans('label.user_work_period') !!}">
                @if ($errors->has('user_work_period'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_work_period') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('user_placement_location') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.user_placement_location') !!}</label>
                <input type="text" class="form-control" readonly="true" name="user_placement_location" value="{!! old('user_placement_location')!==null ? old('user_placement_location') : $data->data->user_placement_location !!}" placeholder="{!! trans('label.user_placement_location') !!}">
                @if ($errors->has('user_placement_location'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_placement_location') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('company_hrd_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.company_hrd_name') !!}</label>
                <input type="text" class="form-control" readonly="true" name="company_hrd_name" value="{!! old('company_hrd_name')!==null ? old('company_hrd_name') : $data->data->company_hrd_name !!}" placeholder="{!! trans('label.company_hrd_name') !!}">
                @if ($errors->has('company_hrd_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('company_hrd_name') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group{{ $errors->has('company_industry') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.company_industry') !!}</label>
                <input type="text" class="form-control" readonly="true" name="company_industry" value="{!! old('company_industry')!==null ? old('company_industry') : $data->data->company_industry !!}" placeholder="{!! trans('label.company_industry') !!}">
                @if ($errors->has('company_industry'))
                    <span class="help-block">
                    <strong>{{ $errors->first('company_industry') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('company_call_number') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.company_call_number') !!}</label>
                <input type="text" class="form-control" readonly="true" name="company_call_number" value="{!! old('company_call_number')!==null ? old('company_call_number') : $data->data->company_call_number !!}" placeholder="{!! trans('label.company_call_number') !!}">
                @if ($errors->has('company_call_number'))
                    <span class="help-block">
                    <strong>{{ $errors->first('company_call_number') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group{{ $errors->has('company_bank_payroll') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.company_bank_payroll') !!}</label>
                <input type="text" class="form-control" readonly="true" name="company_bank_payroll" value="{!! old('company_bank_payroll')!==null ? old('company_bank_payroll') : $data->data->company_bank_payroll !!}" placeholder="{!! trans('label.company_bank_payroll') !!}">
                @if ($errors->has('company_bank_payroll'))
                    <span class="help-block">
                    <strong>{{ $errors->first('company_bank_payroll') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group{{ $errors->has('interviewee_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.interviewee_name') !!}</label>
                <input type="text" class="form-control" readonly="true" name="interviewee_name" value="{!! old('interviewee_name')!==null ? old('interviewee_name') : $data->data->interviewee_name !!}" placeholder="{!! trans('label.interviewee_name') !!}">
                @if ($errors->has('interviewee_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('interviewee_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('interviewee_employment') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.interviewee_employment') !!}</label>
                <input type="text" class="form-control" readonly="true" name="interviewee_employment" value="{!! old('interviewee_employment')!==null ? old('interviewee_employment') : $data->data->interviewee_employment !!}" placeholder="{!! trans('label.interviewee_employment') !!}">
                @if ($errors->has('interviewee_employment'))
                    <span class="help-block">
                    <strong>{{ $errors->first('interviewee_employment') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.position') !!}</label>
                <input type="text" class="form-control" readonly="true" name="position" value="{!! old('position')!==null ? old('position') : $data->data->position !!}" placeholder="{!! trans('label.position') !!}">
                @if ($errors->has('position'))
                    <span class="help-block">
                    <strong>{{ $errors->first('position') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
        @if($data->data->is_offline)

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">{!! trans('label.submission') !!} {!! trans('label.is_offline') !!}</label>
                    <input type="text" class="form-control" style="background: #00a157;color: #d9edf7" value="ONLINE SUBMISSION" readonly="true">
                </div>
            </div>
        @endif
        @if(!$data->data->is_offline)

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">{!! trans('label.submission') !!} {!! trans('label.is_offline') !!}</label>
                    <input type="text" class="form-control" style="background: #9f191f;color: #d9edf7" value="OFFLINE SUBMISSION" readonly="true">
                </div>
            </div>
        @endif
    </div>
    <br>
    <div class="modal-header modal-warning">
        <h4 class="modal-title">Work Location Detail</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('province_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.province') !!}</label>
                <input type="text" class="form-control" readonly="true" name="province_name" value="{!! old('province_name')!==null ? old('province_name') : $data->data->province_name !!}" placeholder="{!! trans('label.province') !!}">
                @if ($errors->has('province_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('province_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('city_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.city') !!}</label>
                <input type="text" class="form-control" readonly="true" name="city_name" value="{!! old('city_name')!==null ? old('city_name') : $data->data->city_name !!}" placeholder="{!! trans('label.city') !!}">
                @if ($errors->has('city_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('city_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('district_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.district') !!}</label>
                <input type="text" class="form-control" readonly="true" name="district_name" value="{!! old('district_name')!==null ? old('district_name') : $data->data->district_name !!}" placeholder="{!! trans('label.district') !!}">
                @if ($errors->has('district_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('district_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('work_location') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.work_location') !!}</label>
                <input type="text" class="form-control" readonly="true" name="work_location" value="{!! old('work_location')!==null ? old('work_location') : $data->data->work_location !!}" placeholder="{!! trans('label.district') !!}">
                @if ($errors->has('work_location'))
                    <span class="help-block">
                    <strong>{{ $errors->first('work_location') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
    </div>
    <br>
    <div class="modal-header modal-warning">
        <h4 class="modal-title">Emergency Contact</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('econ_name') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.econ_name') !!}</label>
                <input type="text" class="form-control" readonly="true" name="econ_name" value="{!! old('econ_name')!==null ? old('econ_name') : $data->data->econ_name !!}" placeholder="{!! trans('label.econ_name') !!}">
                @if ($errors->has('econ_name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('econ_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('econ_relationship') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.econ_relationship') !!}</label>
                <input type="text" class="form-control" readonly="true" name="econ_relationship" value="{!! old('econ_relationship')!==null ? old('econ_relationship') : $data->data->econ_relationship !!}" placeholder="{!! trans('label.econ_relationship') !!}">
                @if ($errors->has('econ_relationship'))
                    <span class="help-block">
                    <strong>{{ $errors->first('econ_relationship') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('econ_address') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.econ_address') !!}</label>
                <input type="text" class="form-control" readonly="true" name="econ_address" value="{!! old('econ_address')!==null ? old('econ_address') : $data->data->econ_address !!}" placeholder="{!! trans('label.econ_address') !!}">
                @if ($errors->has('econ_address'))
                    <span class="help-block">
                    <strong>{{ $errors->first('econ_address') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('econ_phone') ? ' has-error' : '' }}">
                <label class="control-label">{!! trans('label.econ_phone') !!}</label>
                <input type="text" class="form-control" readonly="true" name="econ_phone" value="{!! old('econ_phone')!==null ? old('econ_phone') : $data->data->econ_phone !!}" placeholder="{!! trans('label.econ_phone') !!}">
                @if ($errors->has('econ_phone'))
                    <span class="help-block">
                    <strong>{{ $errors->first('econ_phone') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
    <br>
    <div class="modal-header modal-warning">
        <h4 class="modal-title">Proof of Salary (Slip Gaji)</h4>
    </div>
    <br>
    <div class="row" align="center">
        <div>
            <div class="form-group">
                <div class="form-group">
                    <hr>
                    @if($data->data->proof_of_salary)
                        <img align="center" src="{!! $data->data->proof_of_salary !!}" height="50%" width="50%">
                    @endif
                    @if(!$data->data->proof_of_salary)
                        <img align="center" src="{{URL::asset('/img/not-found.png')}}" alt="profile Pic" height="200" width="200">
                    @endif
                </div>
            </div>
        </div>
        <input type="hidden" class="form-control" readonly="true" name="proof_of_salary" value="{!! old('proof_of_salary')!==null ? old('proof_of_salary') : $data->data->proof_of_salary !!}" placeholder="{!! trans('label.district') !!}">
        <input type="hidden" class="form-control" readonly="true" name="id_submission" value="{!! old('id_submission')!==null ? old('id_submission') : $data->data->id_submission !!}" placeholder="{!! trans('label.district') !!}">
        <input type="hidden" class="form-control" readonly="true" name="province_id" value="{!! old('province_id')!==null ? old('province_id') : $data->data->province_id !!}" placeholder="{!! trans('label.province') !!}">
        <input type="hidden" class="form-control" readonly="true" name="city_id" value="{!! old('city_id')!==null ? old('city_id') : $data->data->city_id !!}" placeholder="{!! trans('label.city') !!}">
        <input type="hidden" class="form-control" readonly="true" name="district_id" value="{!! old('district_id')!==null ? old('district_id') : $data->data->district_id !!}" placeholder="{!! trans('label.district') !!}">

    </div>
    <hr>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-warning">{!! trans('button.move') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}
