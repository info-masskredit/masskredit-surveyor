{!! Form::open(['route' => [$route.'.update', encodeids($data->id)], 'method' => 'PUT']) !!}
<div class="box-body">
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.surveyor') !!}</label>
            {!! Form::viewText($surveyors) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class="control-label">{!! trans('label.status').trans('icon.mandatory') !!}</label>
            <select class="form-control select2" name="status" data-placeholder="{!! trans('label.status') !!}" autofocus>
                <option></option>
                @foreach ($status as $status)
                    <option value="{!! $status->id !!}"{!! old('status') ? (old('status')==$status->id ? ' selected' : '') : ($data->status_id == $status->id ? ' selected' : '') !!}>{!! $status->name !!}</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{!! $errors->first('status') !!}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.survey') !!} {!! trans('label.field') !!} {!! trans('label.status') !!}</label>
            {!! Form::viewText($fieldStatus) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.survey') !!} {!! trans('label.phone') !!} {!! trans('label.status') !!}</label>
            {!! Form::viewText($phoneStatus) !!}
        </div>
    </div>
</div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-warning">{!! trans('button.edit') !!}</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('button.close') !!}</button>
</div>
{!! Form::close() !!}