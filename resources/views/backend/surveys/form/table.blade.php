<div class="box-body">
    <table class="table table-striped table-bordered table-datatables" width="100%">
        <thead>
            <tr>
                <th>{!! trans('label.no') !!}</th>
                <th>{!! trans('label.user_name') !!}</th>
                <th>{!! trans('label.user_employment') !!}</th>
                <th>{!! trans('label.user_phone_number') !!}</th>
                <th>{!! trans('label.surveyor') !!}</th>
                <th>{!! trans('label.status_name') !!}</th>
                <th>{!! trans('label.action') !!}</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

