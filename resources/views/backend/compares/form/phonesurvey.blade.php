{!! Form::open(['route' => [$route.'.updatephone', encodeids($byphone->id)], 'method' => 'PUT']) !!}

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-body box-profile">
            <h3 align="center">
                <i class="fa fa-phone-square"></i> &nbsp; Survey From Phone
            </h3>
            <hr>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_name') !!}</label>
                            <input type="text" class="form-control" name="user_name"
                                   value="{!! old('user_name')!==null ? old('user_name') : $byphone->user_name !!}"
                                   placeholder="{!! trans('label.user_name') !!}">
                            @if ($errors->has('user_name'))

                                <span class="help-block">
										<strong>{{ $errors->first('user_name') }}</strong>
									</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_nik') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_nik') !!}</label>
                            <input type="text" class="form-control" name="user_nik"
                                   value="{!! old('user_nik')!==null ? old('user_nik') : $byphone->user_nik !!}"
                                   placeholder="{!! trans('label.user_nik') !!}">
                            @if ($errors->has('user_nik'))

                                <span class="help-block">
											<strong>{{ $errors->first('user_nik') }}</strong>
										</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_employment') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_employment') !!}</label>
                            <input type="text" class="form-control" name="user_employment"
                                   value="{!! old('user_employment')!==null ? old('user_employment') : $byphone->user_employment !!}"
                                   placeholder="{!! trans('label.user_employment') !!}">
                            @if ($errors->has('user_employment'))

                                <span class="help-block">
												<strong>{{ $errors->first('user_employment') }}</strong>
											</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_phone_number') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_phone_number') !!}</label>
                            <input type="text" class="form-control" name="user_phone_number"
                                   value="{!! old('user_phone_number')!==null ? old('user_phone_number') : $byphone->user_phone_number !!}"
                                   placeholder="{!! trans('label.user_phone_number') !!}">
                            @if ($errors->has('user_phone_number'))

                                <span class="help-block">
													<strong>{{ $errors->first('user_phone_number') }}</strong>
												</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_work_period') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_work_period') !!}</label>
                            <input type="text" class="form-control" name="user_work_period"
                                   value="{!! old('user_work_period')!==null ? old('user_work_period') : $byphone->user_work_period !!}"
                                   placeholder="{!! trans('label.user_work_period') !!}">
                            @if ($errors->has('user_work_period'))

                                <span class="help-block">
														<strong>{{ $errors->first('user_work_period') }}</strong>
													</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('user_placement_location_2') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.user_placement_location_2') !!}</label>
                            <input type="text" class="form-control" name="user_placement_location_2"
                                   value="{!! old('user_placement_location_2')!==null ? old('user_placement_location_2') : $byphone->user_placement_location_2 !!}"
                                   placeholder="{!! trans('label.user_placement_location_2') !!}">
                            @if ($errors->has('user_placement_location_2'))

                                <span class="help-block">
															<strong>{{ $errors->first('user_placement_location_2') }}</strong>
														</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('company_hrd_name') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.company_hrd_name') !!}</label>
                            <input type="text" class="form-control" name="company_hrd_name"
                                   value="{!! old('company_hrd_name')!==null ? old('company_hrd_name') : $byphone->company_hrd_name !!}"
                                   placeholder="{!! trans('label.company_hrd_name') !!}">
                            @if ($errors->has('company_hrd_name'))

                                <span class="help-block">
																<strong>{{ $errors->first('company_hrd_name') }}</strong>
															</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('company_industry') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.company_industry') !!}</label>
                            <input type="text" class="form-control" name="company_industry"
                                   value="{!! old('company_industry')!==null ? old('company_industry') : $byphone->company_industry !!}"
                                   placeholder="{!! trans('label.company_industry') !!}">
                            @if ($errors->has('company_industry'))

                                <span class="help-block">
																	<strong>{{ $errors->first('company_industry') }}</strong>
																</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('company_call_number') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.company_call_number') !!}</label>
                            <input type="text" class="form-control" name="company_call_number"
                                   value="{!! old('company_call_number')!==null ? old('company_call_number') : $byphone->company_call_number !!}"
                                   placeholder="{!! trans('label.company_call_number') !!}">
                            @if ($errors->has('company_call_number'))

                                <span class="help-block">
																		<strong>{{ $errors->first('company_call_number') }}</strong>
																	</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('company_bank_payroll') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.company_bank_payroll') !!}</label>
                            <input type="text" class="form-control" name="company_bank_payroll"
                                   value="{!! old('company_bank_payroll')!==null ? old('company_bank_payroll') : $byphone->company_bank_payroll !!}"
                                   placeholder="{!! trans('label.company_bank_payroll') !!}">
                            @if ($errors->has('company_bank_payroll'))

                                <span class="help-block">
																			<strong>{{ $errors->first('company_bank_payroll') }}</strong>
																		</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('interviewee_name') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.interviewee_name') !!}</label>
                            <input type="text" class="form-control" name="interviewee_name"
                                   value="{!! old('interviewee_name')!==null ? old('interviewee_name') : $byphone->interviewee_name !!}"
                                   placeholder="{!! trans('label.interviewee_name') !!}">
                            @if ($errors->has('interviewee_name'))

                                <span class="help-block">
																				<strong>{{ $errors->first('interviewee_name') }}</strong>
																			</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('interviewee_employment') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.interviewee_employment') !!}</label>
                            <input type="text" class="form-control" name="interviewee_employment"
                                   value="{!! old('interviewee_employment')!==null ? old('interviewee_employment') : $byphone->interviewee_employment !!}"
                                   placeholder="{!! trans('label.interviewee_employment') !!}">
                            @if ($errors->has('interviewee_employment'))

                                <span class="help-block">
																					<strong>{{ $errors->first('interviewee_employment') }}</strong>
																				</span>
                            @endif

                        </div>
                    </div>
                </div>
                <hr>
                <div style="width: 100%; height: 20px; border-bottom: 1px solid black; text-align: center">
  <span style="font-size: 20px; background-color: #FFFFFF; padding: 0 10px;">
    Location Detail <!--Padding is optional-->
  </span>
                </div><br><br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('work_location') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.work_location') !!}</label>
                            <input type="text" class="form-control" name="work_location"
                                   value="{!! old('work_location')!==null ? old('work_location') : $byphone->work_location !!}"
                                   placeholder="{!! trans('label.work_location') !!}">
                            @if ($errors->has('work_location'))

                                <span class="help-block">
																					<strong>{{ $errors->first('work_location') }}</strong>
																				</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('econ_address') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.econ_address') !!}</label>
                            <input type="text" class="form-control" name="econ_address"
                                   value="{!! old('econ_address')!==null ? old('econ_address') : $byphone->econ_address !!}"
                                   placeholder="{!! trans('label.econ_address') !!}">
                            @if ($errors->has('econ_address'))

                                <span class="help-block">
																					<strong>{{ $errors->first('econ_address') }}</strong>
																				</span>
                            @endif

                        </div>
                    </div>

                </div>
                <hr>
                <div style="width: 100%; height: 20px; border-bottom: 1px solid black; text-align: center">
  <span style="font-size: 20px; background-color: #FFFFFF; padding: 0 10px;">
    Emergency Contact <!--Padding is optional-->
  </span>
                </div><br><br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('econ_name') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.econ_name') !!}</label>
                            <input type="text" class="form-control" name="econ_name"
                                   value="{!! old('econ_name')!==null ? old('econ_name') : $byphone->econ_name !!}"
                                   placeholder="{!! trans('label.econ_name') !!}">
                            @if ($errors->has('econ_name'))

                                <span class="help-block">
																					<strong>{{ $errors->first('econ_name') }}</strong>
																				</span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('econ_phone') ? ' has-error' : '' }}">
                            <label class="control-label">{!! trans('label.econ_phone') !!}</label>
                            <input type="text" class="form-control" name="econ_phone"
                                   value="{!! old('econ_phone')!==null ? old('econ_phone') : $byphone->econ_phone !!}"
                                   placeholder="{!! trans('label.econ_phone') !!}">
                            @if ($errors->has('econ_phone'))

                                <span class="help-block">
																					<strong>{{ $errors->first('econ_phone') }}</strong>
																				</span>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-warning">{!! trans('button.save') !!}</button>
            </div>


        </div>
    </div>
</div>
{!! Form::close() !!}