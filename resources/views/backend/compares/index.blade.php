@extends('backend.layouts.app')

@section('title', $menu.' | ')

@section('content')
    <section class="content-header">
        <h1>{!! $menu !!}</h1>
        {!! $breadcrumb !!}
    </section>
    <section class="content">
        <div class="row">
            @include($view.'.form.phonesurvey')
            @include($view.'.form.fieldsurvey')
           
        </div>
        <a href="/admin/survey"></a>
        <a href="/admin/survey" class="btn btn-danger" style="margin:1px auto; text-align:center; display:block; width:120px;" >Cancel</a>

    </section>

@endsection

@push('scripts')
    @include($view.'.scripts')
@endpush
