<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_name') !!}</label>
            {!! Form::viewText($data->user_name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_nik') !!}</label>
            {!! Form::viewText($data->user_nik) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_employment') !!}</label>
            {!! Form::viewText($data->user_employment) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_phone_number') !!}</label>
            {!! Form::viewText($data->user_phone_number) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_work_period') !!}</label>
            {!! Form::viewText($data->user_work_period) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.user_placement_location') !!}</label>
            {!! Form::viewText($data->user_placement_location) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.company_hrd_name') !!}</label>
            {!! Form::viewText($data->company_hrd_name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.company_industry') !!}</label>
            {!! Form::viewText($data->company_industry) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.company_call_number') !!}</label>
            {!! Form::viewText($data->company_call_number) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.company_bank_payroll') !!}</label>
            {!! Form::viewText($data->company_bank_payroll) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.interviewee_name') !!}</label>
            {!! Form::viewText($data->interviewee_name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.interviewee_employment') !!}</label>
            {!! Form::viewText($data->interviewee_employment) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.position') !!}</label>
            {!! Form::viewText($data->position) !!}
        </div>
    </div>
    @if($data->is_offline)

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.submission') !!} {!! trans('label.is_offline') !!}</label>
                <input type="text" class="form-control" style="background: #00a157;color: #d9edf7" value="ONLINE SUBMISSION" readonly="true">
            </div>
        </div>
    @endif
    @if(!$data->is_offline)

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">{!! trans('label.submission') !!} {!! trans('label.is_offline') !!}</label>
                <input type="text" class="form-control" style="background: #9f191f;color: #d9edf7" value="OFFLINE SUBMISSION" readonly="true">
            </div>
        </div>
    @endif
</div>
<hr>
<div class="modal-header modal-success">
    <h4 class="modal-title">Work Location Detail</h4>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.province') !!}</label>
            {!! Form::viewText($data->provinces) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.city') !!}</label>
            {!! Form::viewText($data->cities) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.district') !!}</label>
            {!! Form::viewText($data->districts) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.work_location') !!}</label>
            {!! Form::viewText($data->work_location) !!}
        </div>
    </div>
</div>
<hr>
<div class="modal-header modal-success">
    <h4 class="modal-title">Emergency Contact</h4>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.econ_name') !!}</label>
            {!! Form::viewText($data->econ_name) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.econ_relationship') !!}</label>
            {!! Form::viewText($data->econ_relationship) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.econ_phone') !!}</label>
            {!! Form::viewText($data->econ_phone) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.econ_address') !!}</label>
            {!! Form::viewText($data->econ_address) !!}
        </div>
    </div>
</div>
<hr>
<div class="modal-header modal-success">
    <h4 class="modal-title">Assignee Status</h4>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.status_name') !!}</label>
            {!! Form::viewText($data->status) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">{!! trans('label.surveyor') !!}</label>
            {!! Form::viewText($data->surveyor) !!}
        </div>
    </div>

</div>
<hr>
<div class="modal-header modal-success">
    <h4 class="modal-title">Proof of Salary (Slip Gaji)</h4>
</div>
<br>
<div class="row" align="center">
    <div>
        <div class="form-group">
            <hr>
            @if($data->proof_of_salary)
                <img align="center" src="{!! $data->proof_of_salary !!}" height="50%" width="50%">
            @endif
            @if(!$data->proof_of_salary)
                <img align="center" src="{{URL::asset('/img/not-found.png')}}" alt="profile Pic" height="200" width="200">
            @endif
        </div>
    </div>
</div>

