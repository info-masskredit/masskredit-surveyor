<?php

return [

    'action' => [
        'activate' => ':title has been successfully activated.',
        'change' => ':title has been successfully changed.',
        'create' => ':title has been successfully created.',
        'move' => ':title has been successfully moved to Survey List.',
        'deactivate' => ':title has been successfully deactivated.',
        'delete' => ':title has been successfully deleted.',
        'save' => ':title has been successfully saved.',
        'update' => ':title has been successfully updated.',
        'compare' => 'Survey data has been update by choosing :title survey.',
    ],
    'leave_blank_password' => 'Leave blank if you don\'t to change password',
    'login' => 'You successfully login',
    'register' => 'Congratulation(s) your account has been registered',
    'reset_password' => 'Password your account has been reseted',

    'api' => [
        'success' => 'Operation successfully executed.',
        'error' => 'Error executing request.',
    ]
];
