<?php

return [

    'dashboard' => [
        'name' => 'Dashboard',
    ],
    'home' => [
        'name' => 'Home',
    ],
    'menu' => [
        'name' => 'Menu',
    ],
    'merchant' => [
        'name' => 'Merchant',
    ],
    'profile' => [
        'name' => 'Profile',
    ],
    'role' => [
        'name' => 'Role',
    ],
    'user' => [
        'name' => 'User',
    ],
    'product' => [
        'name' => 'Product',
    ],
    'bank' => [
        'name' => 'Bank',
    ],
    'province' => [
        'name' => 'Province',
    ],
    'city' => [
        'name' => 'City',
    ],
    'district' => [
        'name' => 'District',
    ],
    'surveyor' => [
        'name' => 'Surveyor',
    ],
    'survey' => [
        'name' => 'Survey',
    ],
    'survey_phone' => [
        'name' => 'Survey by Phone',
    ],
    'survey_field' => [
        'name' => 'Survey by Field',
    ],
    'submission' => [
        'name' => 'Submission',
    ],
    'compare' => [
        'name' => 'Compare',
    ],

];
