<?php

return [

    # AAAAA
    'action' => 'Action',
    'active' => 'Active',
    'activate' => 'Activate',
    'activity' => 'Activity',
    'admin' => 'Admin',
    'address' => 'Address',

    # BBBBB
    'bank' => 'Bank',

    # CCCCC
    'create' => 'Create',
    'change' => 'Change',
    'change_password' => 'Change Password',
    'check_all' => 'Check All',
    'code' => 'Code',
    'city' => "City",
    'company_hrd_name' => 'Company HRD Name',
    'company_industry' => 'Company Industry',
    'company_call_number' => 'Company Call Number',
    'company_bank_payroll' => 'Company Bank Payroll',
    'compare' => "Compare",
 
    # DDDDD
    'data' => 'Data',
    'deactivate' => 'Deactivate',
    'delete' => 'Delete',
    'description' => 'Description',
    'description_ph' => 'Description',
    'detail' => 'Detail',
    'district' => 'District',

    # EEEEE
    'edit' => 'Edit',
    'email' => 'Email',
    'email_ph' => 'Email',
    'econ_relationship' => 'Emergency Contact Relationship',
    'econ_name' => 'Emergency Contact Name',
    'econ_address' => 'Emergency Contact Address',
    'econ_phone' => 'Emergency Contact Phone',

    # FFFFF
    'field' => 'Field',
    'fullname' => 'Fullname',
    'fullname_ph' => 'Fullname',

    # HHHHH
    'hide' => 'Hide',

    # IIIII
    'icon' => 'Icon',
    'ip_address' => 'Ip Address',
    'interviewee_name' => 'Interviewee Name',
    'interviewee_employment' => 'Interviewee Employment',
    'is_offline' => 'Type',

    # JJJJJ
    'join_date' => 'Join Date',

    # LLLLL
    'last_login' => 'Last Login',
    'list_of' => 'List of :Name',

    # MMMMM
    'menu' => 'Menu',
    'move' => 'Move',
    'max_survey' => 'Max Survey',

    # NNNNN
    'name' => 'Name',
    'name_ph' => 'Name',
    'no' => 'No',
    'nik' => 'NIK',
    'not_active' => 'Not Active',
    'nama_bank' => 'Nama Bank',

    # OOOOO
    'on_progress_1' => 'ON PROGRESS 1',

    # PPPPP
    'parent' => 'Parent',
    'password' => 'Password',
    'password_ph' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'password_confirmation_ph' => 'Password Confirmation',
    'password_old' => 'Password Old',
    'password_old_ph' => 'Password Old',
    'phone' => 'Phone',
    'phone_ph' => 'Phone',
    'profile' => 'Profile',
    'province' => "Province",
    'phone_number' => "Phone Number",
    'position' => "Position",
    'proof_of_salary' => 'Salary Proof Photo',

    # RRRRR
    'read' => 'Read',
    'remember_me' => 'Remember Me',
    'role' => 'Role',
    'role_privileges' => 'Role Privileges',

    # SSSSS
    'selectbox' => 'Select :Name',
    'sequence' => 'Sequence',
    'sequence_ph' => 'Sequence',
    'show' => 'Show',
    'sidebar' => 'Sidebar',
    'status' => 'Status',
    'status_name' => 'Status',
    'surveyor' => 'Active Surveyor',
    'survey' => 'Survey',
    'submission' => 'Submission',

    # TTTTT
    'title_box_forgot_password' => 'Forgot Password',
    'title_box_login' => 'Login',
    'title_box_register' => 'Register',
    'title_box_reset_password' => 'Reset Password',

    # UUUUU
    'update' => 'Update',
    'url' => 'Url',
    'user_name' => 'User Name',
    'user_nik' => 'User NIK',
    'user_employment' => 'User Employment',
    'user_phone_number' => 'User Phone Number',
    'user_work_period' => 'User Work Period',
    'user_placement_location' => 'User Placement Location (Cabang)',
    'user_placement_location_2' => 'User Placement Location',

    # VVVVV
    'view' => 'View',

    # WWWWW
    'work_location' => 'Work Location/Address',

    # YYYYY
    'yes' => 'Yes',

];
