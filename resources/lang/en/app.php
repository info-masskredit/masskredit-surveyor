<?php

return [

    'company' => 'Masskredit',
    'logo' => 'Dashboard <b>Surveyor</b>',
    'logo_mini' => '<b>DS</b>',
    'title' => 'Dashboard Surveyor',
    'version' => '<b>Version</b> 1.0.0',

];
