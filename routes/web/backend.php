<?php

Route::group(['as' => 'backend::', 'namespace' => 'Backend', 'middleware' => 'auth', 'prefix' => 'admin'], function(){
    Route::get('/', ['as' => 'welcome', function(){
        return redirect()->route('backend::dashboard.index');
    }]);

    Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);

    # ProfileController
    Route::get('profile/{id?}', ['as' => 'profile.index', 'uses' => 'ProfileController@index']);
    Route::put('profile/{id}', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);

    # CompareController
    Route::get('compare/{id?}', ['as' => 'compare.index', 'uses' => 'CompareController@index']);
    Route::put('compare/{id}', ['as' => 'compare.updatephone', 'uses' => 'CompareController@updatephone']);
    Route::put('compare/phone/{id}', ['as' => 'compare.updatefield', 'uses' => 'CompareController@updatefield']);
    Route::put('compare/field/{id}', ['as' => 'compare.update', 'uses' => 'CompareController@update']);

    # UserController
    Route::get('user', ['as' => 'user.index', 'middleware' => 'access:index,admin/user', 'uses' => 'UserController@index']);
    Route::get('user/datatables', ['as' => 'user.datatables', 'middleware' => 'access:index,admin/user', 'uses' => 'UserController@datatables']);
    Route::get('user/detail/{id}', ['as' => 'user.detail', 'middleware' => 'access:detail,admin/user', 'uses' => 'UserController@detail']);
    Route::get('user/create', ['as' => 'user.create', 'middleware' => 'access:create,admin/user', 'uses' => 'UserController@create']);
    Route::post('user/create', ['as' => 'user.store', 'middleware' => 'access:create,admin/user', 'uses' => 'UserController@store']);
    Route::get('user/activate/{id}', ['as' => 'user.activate.get', 'middleware' => 'access:activate,admin/user', 'uses' => 'UserController@getActivate']);
    Route::post('user/activate/{id}', ['as' => 'user.activate.post', 'middleware' => 'access:activate,admin/user', 'uses' => 'UserController@postActivate']);
    Route::get('user/deactivate/{id}', ['as' => 'user.deactivate.get', 'middleware' => 'access:deactivate,admin/user', 'uses' => 'UserController@getDeactivate']);
    Route::post('user/deactivate/{id}', ['as' => 'user.deactivate.post', 'middleware' => 'access:deactivate,admin/user', 'uses' => 'UserController@postDeactivate']);

    # RoleController
    Route::get('role', ['as' => 'role.index', 'middleware' => 'access:index,admin/role', 'uses' => 'RoleController@index']);
    Route::get('role/datatables', ['as' => 'role.datatables', 'middleware' => 'access:index,admin/role', 'uses' => 'RoleController@datatables']);
    Route::get('role/detail/{id}', ['as' => 'role.detail', 'middleware' => 'access:detail,admin/role', 'uses' => 'RoleController@detail']);
    Route::get('role/create', ['as' => 'role.create', 'middleware' => 'access:create,admin/role', 'uses' => 'RoleController@create']);
    Route::post('role/create', ['as' => 'role.store', 'middleware' => 'access:create,admin/role', 'uses' => 'RoleController@store']);
    Route::get('role/edit/{id}', ['as' => 'role.edit', 'middleware' => 'access:update,admin/role', 'uses' => 'RoleController@edit']);
    Route::put('role/edit/{id}', ['as' => 'role.update', 'middleware' => 'access:update,admin/role', 'uses' => 'RoleController@update']);
    Route::get('role/delete/{id}', ['as' => 'role.delete', 'middleware' => 'access:delete,admin/role', 'uses' => 'RoleController@delete']);
    Route::delete('role/delete/{id}', ['as' => 'role.destroy', 'middleware' => 'access:delete,admin/role', 'uses' => 'RoleController@destroy']);

    # MenuController
    Route::get('menu', ['as' => 'menu.index', 'middleware' => 'access:index,admin/menu', 'uses' => 'MenuController@index']);
    Route::get('menu/detail/{id}', ['as' => 'menu.detail', 'middleware' => 'access:detail,admin/menu', 'uses' => 'MenuController@detail']);
    Route::get('menu/create', ['as' => 'menu.create', 'middleware' => 'access:create,admin/menu', 'uses' => 'MenuController@create']);
    Route::post('menu/create', ['as' => 'menu.store', 'middleware' => 'access:create,admin/menu', 'uses' => 'MenuController@store']);
    Route::get('menu/edit/{id}', ['as' => 'menu.edit', 'middleware' => 'access:update,admin/menu', 'uses' => 'MenuController@edit']);
    Route::put('menu/edit/{id}', ['as' => 'menu.update', 'middleware' => 'access:update,admin/menu', 'uses' => 'MenuController@update']);
    Route::get('menu/delete/{id}', ['as' => 'menu.delete', 'middleware' => 'access:delete,admin/menu', 'uses' => 'MenuController@delete']);
    Route::delete('menu/delete/{id}', ['as' => 'menu.destroy', 'middleware' => 'access:delete,admin/menu', 'uses' => 'MenuController@destroy']);

     # ProductController
    Route::get('product', ['as' => 'product.index', 'middleware' => 'access:index,admin/product', 'uses' => 'ProductController@index']);
    Route::get('product/datatables', ['as' => 'product.datatables', 'middleware' => 'access:index,admin/product', 'uses' => 'ProductController@datatables']);
    Route::get('product/detail/{id}', ['as' => 'product.detail', 'middleware' => 'access:detail,admin/product', 'uses' => 'ProductController@detail']);
    Route::get('product/create', ['as' => 'product.create', 'middleware' => 'access:create,admin/product', 'uses' => 'ProductController@create']);
    Route::post('product/create', ['as' => 'product.store', 'middleware' => 'access:create,admin/product', 'uses' => 'ProductController@store']);
    Route::get('product/edit/{id}', ['as' => 'product.edit', 'middleware' => 'access:update,admin/product', 'uses' => 'ProductController@edit']);
    Route::put('product/edit/{id}', ['as' => 'product.update', 'middleware' => 'access:update,admin/product', 'uses' => 'ProductController@update']);
    Route::get('product/delete/{id}', ['as' => 'product.delete', 'middleware' => 'access:delete,admin/product', 'uses' => 'ProductController@delete']);
    Route::delete('product/delete/{id}', ['as' => 'product.destroy', 'middleware' => 'access:delete,admin/product', 'uses' => 'ProductController@destroy']);

    # MerchantController
    Route::get('merchant', ['as' => 'merchant.index', 'middleware' => 'access:index,admin/merchant', 'uses' => 'MerchantController@index']);
    Route::get('merchant/datatables', ['as' => 'merchant.datatables', 'middleware' => 'access:index,admin/merchant', 'uses' => 'MerchantController@datatables']);
    Route::get('merchant/detail/{id}', ['as' => 'merchant.detail', 'middleware' => 'access:detail,admin/merchant', 'uses' => 'MerchantController@detail']);
    Route::get('merchant/create', ['as' => 'merchant.create', 'middleware' => 'access:create,admin/merchant', 'uses' => 'MerchantController@create']);
    Route::post('merchant/create', ['as' => 'merchant.store', 'middleware' => 'access:create,admin/merchant', 'uses' => 'MerchantController@store']);
    Route::get('merchant/edit/{id}', ['as' => 'merchant.edit', 'middleware' => 'access:update,admin/merchant', 'uses' => 'MerchantController@edit']);
    Route::put('merchant/edit/{id}', ['as' => 'merchant.update', 'middleware' => 'access:update,admin/merchant', 'uses' => 'MerchantController@update']);
    Route::get('merchant/activate/{id}', ['as' => 'merchant.activate.get', 'middleware' => 'access:activate,admin/merchant', 'uses' => 'MerchantController@getActivate']);
    Route::post('merchant/activate/{id}', ['as' => 'merchant.activate.post', 'middleware' => 'access:activate,admin/merchant', 'uses' => 'MerchantController@postActivate']);
    Route::get('merchant/deactivate/{id}', ['as' => 'merchant.deactivate.get', 'middleware' => 'access:deactivate,admin/merchant', 'uses' => 'MerchantController@getDeactivate']);
    Route::post('merchant/deactivate/{id}', ['as' => 'merchant.deactivate.post', 'middleware' => 'access:deactivate,admin/merchant', 'uses' => 'MerchantController@postDeactivate']);
    Route::get('merchant/delete/{id}', ['as' => 'merchant.delete', 'middleware' => 'access:delete,admin/merchant', 'uses' => 'MerchantController@delete']);
    Route::delete('merchant/delete/{id}', ['as' => 'merchant.destroy', 'middleware' => 'access:delete,admin/merchant', 'uses' => 'MerchantController@destroy']);

    # BankController
    Route::get('bank', ['as' => 'bank.index', 'middleware' => 'access:index,admin/bank', 'uses' => 'BankController@index']);
    Route::get('bank/datatables', ['as' => 'bank.datatables', 'middleware' => 'access:index,admin/bank', 'uses' => 'BankController@datatables']);
    Route::get('bank/detail/{id}', ['as' => 'bank.detail', 'middleware' => 'access:detail,admin/bank', 'uses' => 'BankController@detail']);
    Route::get('bank/create', ['as' => 'bank.create', 'middleware' => 'access:create,admin/bank', 'uses' => 'BankController@create']);
    Route::post('bank/create', ['as' => 'bank.store', 'middleware' => 'access:create,admin/bank', 'uses' => 'BankController@store']);
    Route::get('bank/edit/{id}', ['as' => 'bank.edit', 'middleware' => 'access:update,admin/bank', 'uses' => 'BankController@edit']);
    Route::put('bank/edit/{id}', ['as' => 'bank.update', 'middleware' => 'access:update,admin/bank', 'uses' => 'BankController@update']);
    Route::get('bank/activate/{id}', ['as' => 'bank.activate.get', 'middleware' => 'access:activate,admin/bank', 'uses' => 'BankController@getActivate']);
    Route::post('bank/activate/{id}', ['as' => 'bank.activate.post', 'middleware' => 'access:activate,admin/bank', 'uses' => 'BankController@postActivate']);
    Route::get('bank/deactivate/{id}', ['as' => 'bank.deactivate.get', 'middleware' => 'access:deactivate,admin/bank', 'uses' => 'BankController@getDeactivate']);
    Route::post('bank/deactivate/{id}', ['as' => 'bank.deactivate.post', 'middleware' => 'access:deactivate,admin/bank', 'uses' => 'BankController@postDeactivate']);
    Route::get('bank/delete/{id}', ['as' => 'bank.delete', 'middleware' => 'access:delete,admin/bank', 'uses' => 'BankController@delete']);
    Route::delete('bank/delete/{id}', ['as' => 'bank.destroy', 'middleware' => 'access:delete,admin/bank', 'uses' => 'BankController@destroy']);

    # ProvinceController
    Route::get('province', ['as' => 'province.index', 'middleware' => 'access:index,admin/province', 'uses' => 'ProvinceController@index']);
    Route::get('province/datatables', ['as' => 'province.datatables', 'middleware' => 'access:index,admin/province', 'uses' => 'ProvinceController@datatables']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::get('province/detail/{id}', ['as' => 'province.detail', 'middleware' => 'access:detail,admin/province', 'uses' => 'ProvinceController@detail']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::get('province/create', ['as' => 'province.create', 'middleware' => 'access:create,admin/province', 'uses' => 'ProvinceController@create']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::post('province/create', ['as' => 'province.store', 'middleware' => 'access:create,admin/province', 'uses' => 'ProvinceController@store']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::get('province/edit/{id}', ['as' => 'province.edit', 'middleware' => 'access:update,admin/province', 'uses' => 'ProvinceController@edit']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::put('province/edit/{id}', ['as' => 'province.update', 'middleware' => 'access:update,admin/province', 'uses' => 'ProvinceController@update']);
    Route::delete('province/delete/{id}', ['as' => 'province.destroy', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@destroy']);
    Route::get('province/delete/{id}', ['as' => 'province.delete', 'middleware' => 'access:delete,admin/province', 'uses' => 'ProvinceController@delete']);

     # CityController
    Route::get('city', ['as' => 'city.index', 'middleware' => 'access:index,admin/city', 'uses' => 'CityController@index']);
    Route::get('city/datatables', ['as' => 'city.datatables', 'middleware' => 'access:index,admin/city', 'uses' => 'CityController@datatables']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::get('city/detail/{id}', ['as' => 'city.detail', 'middleware' => 'access:detail,admin/city', 'uses' => 'CityController@detail']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::get('city/create', ['as' => 'city.create', 'middleware' => 'access:create,admin/city', 'uses' => 'CityController@create']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::post('city/create', ['as' => 'city.store', 'middleware' => 'access:create,admin/city', 'uses' => 'CityController@store']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::get('city/edit/{id}', ['as' => 'city.edit', 'middleware' => 'access:update,admin/city', 'uses' => 'CityController@edit']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::put('city/edit/{id}', ['as' => 'city.update', 'middleware' => 'access:update,admin/city', 'uses' => 'CityController@update']);
    Route::delete('city/delete/{id}', ['as' => 'city.destroy', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@destroy']);
    Route::get('city/delete/{id}', ['as' => 'city.delete', 'middleware' => 'access:delete,admin/city', 'uses' => 'CityController@delete']);

    # DistrictController
    Route::get('district', ['as' => 'district.index', 'middleware' => 'access:index,admin/district', 'uses' => 'DistrictController@index']);
    Route::get('district/datatables', ['as' => 'district.datatables', 'middleware' => 'access:index,admin/district', 'uses' => 'DistrictController@datatables']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::get('district/detail/{id}', ['as' => 'district.detail', 'middleware' => 'access:detail,admin/district', 'uses' => 'DistrictController@detail']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::get('district/create', ['as' => 'district.create', 'middleware' => 'access:create,admin/district', 'uses' => 'DistrictController@create']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::post('district/create', ['as' => 'district.store', 'middleware' => 'access:create,admin/district', 'uses' => 'DistrictController@store']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::get('district/edit/{id}', ['as' => 'district.edit', 'middleware' => 'access:update,admin/district', 'uses' => 'DistrictController@edit']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::put('district/edit/{id}', ['as' => 'district.update', 'middleware' => 'access:update,admin/district', 'uses' => 'DistrictController@update']);
    Route::delete('district/delete/{id}', ['as' => 'district.destroy', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@destroy']);
    Route::get('district/delete/{id}', ['as' => 'district.delete', 'middleware' => 'access:delete,admin/district', 'uses' => 'DistrictController@delete']);

    # SurveyorController
    Route::get('surveyor', ['as' => 'surveyor.index', 'middleware' => 'access:index,admin/surveyor', 'uses' => 'SurveyorController@index']);
    Route::get('surveyor/datatables', ['as' => 'surveyor.datatables', 'middleware' => 'access:index,admin/surveyor', 'uses' => 'SurveyorController@datatables']);
    Route::get('surveyor/datatables2/{id}', ['as' => 'surveyor.datatables2', 'middleware' => 'access:index,admin/surveyor', 'uses' => 'SurveyorController@datatables2']);
    Route::get('surveyor/detail/{id}', ['as' => 'surveyor.detail', 'middleware' => 'access:detail,admin/surveyor', 'uses' => 'SurveyorController@detail']);
    Route::get('surveyor/create', ['as' => 'surveyor.create', 'middleware' => 'access:create,admin/surveyor', 'uses' => 'SurveyorController@create']);
    Route::post('surveyor/create', ['as' => 'surveyor.store', 'middleware' => 'access:create,admin/surveyor', 'uses' => 'SurveyorController@store']);
    Route::get('surveyor/edit/{id}', ['as' => 'surveyor.edit', 'middleware' => 'access:update,admin/surveyor', 'uses' => 'SurveyorController@edit']);
    Route::put('surveyor/edit/{id}', ['as' => 'surveyor.update', 'middleware' => 'access:update,admin/surveyor', 'uses' => 'SurveyorController@update']);
    Route::get('surveyor/delete/{id}', ['as' => 'surveyor.delete', 'middleware' => 'access:delete,admin/surveyor', 'uses' => 'SurveyorController@delete']);
    Route::delete('surveyor/delete/{id}', ['as' => 'surveyor.destroy', 'middleware' => 'access:delete,admin/surveyor', 'uses' => 'SurveyorController@destroy']);

    # SurveyController
    Route::get('survey', ['as' => 'survey.index', 'middleware' => 'access:index,admin/survey', 'uses' => 'SurveyController@index']);
    Route::get('survey/datatables', ['as' => 'survey.datatables', 'middleware' => 'access:index,admin/survey', 'uses' => 'SurveyController@datatables']);
    Route::get('survey/detail/{id}', ['as' => 'survey.detail', 'middleware' => 'access:detail,admin/survey', 'uses' => 'SurveyController@detail']);
    Route::get('survey/create', ['as' => 'survey.create', 'middleware' => 'access:create,admin/survey', 'uses' => 'SurveyController@create']);
    Route::post('survey/create', ['as' => 'survey.store', 'middleware' => 'access:create,admin/survey', 'uses' => 'SurveyController@store']);
    Route::get('survey/edit/{id}', ['as' => 'survey.edit', 'middleware' => 'access:update,admin/survey', 'uses' => 'SurveyController@edit']);
    Route::put('survey/edit/{id}', ['as' => 'survey.update', 'middleware' => 'access:update,admin/survey', 'uses' => 'SurveyController@update']);
    Route::get('survey/delete/{id}', ['as' => 'survey.delete', 'middleware' => 'access:delete,admin/survey', 'uses' => 'SurveyController@delete']);
    Route::delete('survey/delete/{id}', ['as' => 'survey.destroy', 'middleware' => 'access:delete,admin/survey', 'uses' => 'SurveyController@destroy']);

    # SurveyByPhoneController
    Route::get('survey_phone', ['as' => 'survey_phone.index', 'middleware' => 'access:index,admin/survey_phone', 'uses' => 'SurveyByPhoneController@index']);
    Route::get('survey_phone/datatables', ['as' => 'survey_phone.datatables', 'middleware' => 'access:index,admin/survey_phone', 'uses' => 'SurveyByPhoneController@datatables']);
    Route::get('survey_phone/detail/{id}', ['as' => 'survey_phone.detail', 'middleware' => 'access:detail,admin/survey_phone', 'uses' => 'SurveyByPhoneController@detail']);
    Route::get('survey_phone/create', ['as' => 'survey_phone.create', 'middleware' => 'access:create,admin/survey_phone', 'uses' => 'SurveyByPhoneController@create']);
    Route::post('survey_phone/create', ['as' => 'survey_phone.store', 'middleware' => 'access:create,admin/survey_phone', 'uses' => 'SurveyByPhoneController@store']);
    Route::get('survey_phone/edit/{id}', ['as' => 'survey_phone.edit', 'middleware' => 'access:update,admin/survey_phone', 'uses' => 'SurveyByPhoneController@edit']);
    Route::put('survey_phone/edit/{id}', ['as' => 'survey_phone.update', 'middleware' => 'access:update,admin/survey_phone', 'uses' => 'SurveyByPhoneController@update']);
    Route::get('survey_phone/delete/{id}', ['as' => 'survey_phone.delete', 'middleware' => 'access:delete,admin/survey_phone', 'uses' => 'SurveyByPhoneController@delete']);
    Route::delete('survey_phone/delete/{id}', ['as' => 'survey_phone.destroy', 'middleware' => 'access:delete,admin/survey_phone', 'uses' => 'SurveyByPhoneController@destroy']);

    # SurveyByFieldController
    Route::get('survey_field', ['as' => 'survey_field.index', 'middleware' => 'access:index,admin/survey_field', 'uses' => 'SurveyByFieldController@index']);
    Route::get('survey_field/datatables', ['as' => 'survey_field.datatables', 'middleware' => 'access:index,admin/survey_field', 'uses' => 'SurveyByFieldController@datatables']);
    Route::get('survey_field/detail/{id}', ['as' => 'survey_field.detail', 'middleware' => 'access:detail,admin/survey_field', 'uses' => 'SurveyByFieldController@detail']);
    Route::get('survey_field/create', ['as' => 'survey_field.create', 'middleware' => 'access:create,admin/survey_field', 'uses' => 'SurveyByFieldController@create']);
    Route::post('survey_field/create', ['as' => 'survey_field.store', 'middleware' => 'access:create,admin/survey_field', 'uses' => 'SurveyByFieldController@store']);
    Route::get('survey_field/edit/{id}', ['as' => 'survey_field.edit', 'middleware' => 'access:update,admin/survey_field', 'uses' => 'SurveyByFieldController@edit']);
    Route::put('survey_field/edit/{id}', ['as' => 'survey_field.update', 'middleware' => 'access:update,admin/survey_field', 'uses' => 'SurveyByFieldController@update']);
    Route::get('survey_field/delete/{id}', ['as' => 'survey_field.delete', 'middleware' => 'access:delete,admin/survey_field', 'uses' => 'SurveyByFieldController@delete']);
    Route::delete('survey_field/delete/{id}', ['as' => 'survey_field.destroy', 'middleware' => 'access:delete,admin/survey_field', 'uses' => 'SurveyByFieldController@destroy']);

    # SubmissionController
    Route::get('submission', ['as' => 'submission.index', 'middleware' => 'access:index,admin/submission', 'uses' => 'SubmissionController@index']);
    Route::get('submission/datatables', ['as' => 'submission.datatables', 'middleware' => 'access:index,admin/submission', 'uses' => 'SubmissionController@datatables']);
    Route::get('submission/detail/{id}', ['as' => 'submission.detail', 'middleware' => 'access:detail,admin/submission', 'uses' => 'SubmissionController@detail']);
    Route::get('submission/create', ['as' => 'submission.create', 'middleware' => 'access:create,admin/submission', 'uses' => 'SubmissionController@create']);
    Route::post('submission/create', ['as' => 'submission.store', 'middleware' => 'access:create,admin/submission', 'uses' => 'SubmissionController@store']);
    Route::get('submission/edit/{id}', ['as' => 'submission.edit', 'middleware' => 'access:update,admin/submission', 'uses' => 'SubmissionController@edit']);
    Route::put('submission/edit/{id}', ['as' => 'submission.update', 'middleware' => 'access:update,admin/submission', 'uses' => 'SubmissionController@update']);
    Route::get('submission/delete/{id}', ['as' => 'submission.delete', 'middleware' => 'access:delete,admin/submission', 'uses' => 'SubmissionController@delete']);
    Route::delete('submission/delete/{id}', ['as' => 'submission.destroy', 'middleware' => 'access:delete,admin/submission', 'uses' => 'SubmissionController@destroy']);
});
