<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api::', 'namespace' => 'Api\Mobile', 'prefix' => 'v1'], function () {
    Route::post('oauth/token', ['as' => 'surveyor.token', 'uses' => 'AccessTokenController@issueToken']);
    Route::delete('oauth/token', ['as' => 'surveyor.token', 'uses' => 'AccessTokenController@revokeToken'])
        ->middleware('auth:api');
});

Route::group(['as' => 'api::', 'namespace' => 'Api\Mobile', 'middleware' => 'auth:api', 'prefix' => 'v1'], function() {
    Route::get('/profmercant', ['as' => 'merchant.profile', 'uses' => 'MerchantController@getProfile']);
    Route::get('/profile', ['as' => 'surveyor.profile', 'uses' => 'SurveyorController@getProfile']);
    Route::get('/survey/list', ['as' => 'survey.list', 'uses' => 'SurveyorController@getListSurvey']);
    Route::post('/survey/fill', ['as' => 'survey.fill', 'uses' => 'SurveyorController@fillSurvey']);
    Route::post('/survey/take', ['as' => 'survey.take', 'uses' => 'SurveyorController@takeSurvey']);
    Route::resource('/images', 'ImagesController', ['only' => ['store', 'destroy']]);
});

Route::group(['as' => 'api::', 'namespace' => 'Api', 'middleware' => 'auth:api', 'prefix' => 'v1'], function() {
    Route::get('merchant', ['as' => 'merchant.index', 'uses' => 'MerchantController@index']);
    Route::get('merchant/detail/{id}', ['as' => 'merchant.detail', 'uses' => 'MerchantController@detail']);
    Route::post('merchant/store', ['as' => 'merchant.store', 'uses' => 'MerchantController@store']);
    Route::post('merchant/update/{id}', ['as' => 'merchant.update', 'uses' => 'MerchantController@update']);
    Route::delete('merchant/destroy/{id}', ['as' => 'merchant.destroy', 'uses' => 'MerchantController@destroy']);
});