Mass Kredit Surveyor REST & BACKEND
===================================

Buat database PostgreSQL bernama ```mk_survey```

```bash
su postgres -c "createdb 'mk_survey'"
```
Copy paste .env.example menjadi 2 file .env dan .local.env.
Hapus semua isi .env lalu ganti dengan 1 line : 
```local```

Isi .local.env dengan line yang sama dari .env.example dengan setup database yang berbeda
contoh :

```bash
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=mk_survey
DB_USERNAME=postgres
DB_PASSWORD=postgres
```

Jalankan aplikasi dengan command

```bash
php artisan migrate --seed
php artisan serve
```

Buka browser anda dan navigasikan ke ```http://127.0.0.1:8000 atau http://localhost:8000 ```

Happy Coding!