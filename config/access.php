<?php

return [

    'delimiter' => ',',

    /**
     * Menu action list that can be have.
     */
    'menu' => [
        'admin/user' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'activate', 'deactivate'],
        ],
        'admin/role' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/menu' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
         'admin/product' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/merchant' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'activate', 'deactivate', 'delete'],
        ],
        'admin/bank' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/province' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/city' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/district' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/surveyor' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/survey' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/survey_phone' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/survey_field' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
        'admin/submission' => [
            'index'  => 'index',
            'action' => ['index', 'detail', 'create', 'update', 'delete'],
        ],
    ],

];
