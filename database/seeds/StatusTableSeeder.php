<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    protected $statusModel;

    public function __construct()
    {
        $this->statusModel = new \App\Models\Status;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->statusModel->create(['id' => 1, 'name' => 'NEW']);
        $this->statusModel->create(['id' => 2, 'name' => 'ON PROGRESS']);
        $this->statusModel->create(['id' => 3, 'name' => 'ARRIVED']);
        $this->statusModel->create(['id' => 4, 'name' => 'REJECTED']);
        $this->statusModel->create(['id' => 5, 'name' => 'ON CALL']);
        $this->statusModel->create(['id' => 6, 'name' => 'READY TO COMPARE']);
        $this->statusModel->create(['id' => 7, 'name' => 'ASSIGNED']);
        $this->statusModel->create(['id' => 8, 'name' => 'APPROVED']);
        $this->statusModel->create(['id' => 9, 'name' => 'ON THE WAY']);



    }
}
