<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    protected $bankModel;

    public function __construct()
    {
        $this->bankModel = new \App\Models\Bank;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->bankModel->create(['id' => 1, 'code' => '014', 'name' => 'BCA']);
        $this->bankModel->create(['id' => 2, 'code' => '008', 'name' => 'Mandiri']);
        $this->bankModel->create(['id' => 3, 'code' => '011', 'name' => 'Danamon']);
        $this->bankModel->create(['id' => 4, 'code' => '153', 'name' => 'Sinarmas']);
        $this->bankModel->create(['id' => 5, 'code' => '009', 'name' => 'BNI']);
        $this->bankModel->create(['id' => 6, 'code' => '022', 'name' => 'CIMB Niaga']);
    }
}
