<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: riyanpramana
 * Date: 25/07/18
 * Time: 15.06
 */

class DummyLocationSeeder extends Seeder
{
    protected $provinceModel;
    protected $cityModel;
    protected $districtModel;
    protected $surveyorModel;

    public function __construct()
    {
        $this->provinceModel = new \App\Models\Province;
        $this->cityModel = new \App\Models\City;
        $this->districtModel = new \App\Models\District;
        $this->surveyorModel = new \App\Models\Surveyor;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->provinceModel->create(['id' => 0, 'name' => 'Luar Jakarta']);
        $this->provinceModel->create(['id' => 6, 'name' => 'DKI Jakarta']);

        $this->cityModel->create(['id' => 0, 'province_id'=>6, 'name' => 'Luar Jakarta']);

        $this->cityModel->create(['id' => 41, 'province_id'=>6, 'name' => 'Kota Administrasi Jakarta Pusat']);
        $this->districtModel->create(['id' => 422, 'city_id'=>41, 'province_id'=>6, 'name' => 'Kemayoran']);
        $this->districtModel->create(['id' => 421, 'city_id'=>41, 'province_id'=>6, 'name' => 'Johar Baru']);
        $this->districtModel->create(['id' => 423, 'city_id'=>41, 'province_id'=>6, 'name' => 'Menteng']);
        $this->districtModel->create(['id' => 420, 'city_id'=>41, 'province_id'=>6, 'name' => 'Gambir']);
        $this->districtModel->create(['id' => 426, 'city_id'=>41, 'province_id'=>6, 'name' => 'Tanah Abang']);
        $this->districtModel->create(['id' => 425, 'city_id'=>41, 'province_id'=>6, 'name' => 'Senen']);
        $this->districtModel->create(['id' => 419, 'city_id'=>41, 'province_id'=>6, 'name' => 'Cempaka Putih']);
        $this->districtModel->create(['id' => 424, 'city_id'=>41, 'province_id'=>6, 'name' => 'Sawah Besar']);
        $this->districtModel->create(['id' => 418, 'city_id'=>41, 'province_id'=>6, 'name' => 'Jakarta Pusat']);

        $this->cityModel->create(['id' => 40, 'province_id'=>6, 'name' => 'Kota Administrasi Jakarta Barat']);
        $this->districtModel->create(['id' => 427, 'city_id'=>40, 'province_id'=>6, 'name' => 'Jakarta Barat']);
        $this->districtModel->create(['id' => 428, 'city_id'=>40, 'province_id'=>6, 'name' => 'Grogol']);
        $this->districtModel->create(['id' => 432, 'city_id'=>40, 'province_id'=>6, 'name' => 'Palmerah']);
        $this->districtModel->create(['id' => 431, 'city_id'=>40, 'province_id'=>6, 'name' => 'Kembangan']);
        $this->districtModel->create(['id' => 435, 'city_id'=>40, 'province_id'=>6, 'name' => 'Cengkareng']);
        $this->districtModel->create(['id' => 430, 'city_id'=>40, 'province_id'=>6, 'name' => 'Kebon Jeruk']);
        $this->districtModel->create(['id' => 434, 'city_id'=>40, 'province_id'=>6, 'name' => 'Tambora']);
        $this->districtModel->create(['id' => 429, 'city_id'=>40, 'province_id'=>6, 'name' => 'Kalideres']);
        $this->districtModel->create(['id' => 433, 'city_id'=>40, 'province_id'=>6, 'name' => 'Taman Sari']);

        $this->cityModel->create(['id' => 43, 'province_id'=>6, 'name' => 'Kota Administrasi Jakarta Utara']);
        $this->districtModel->create(['id' => 445, 'city_id'=>43, 'province_id'=>6, 'name' => 'Cilincing']);
        $this->districtModel->create(['id' => 444, 'city_id'=>43, 'province_id'=>6, 'name' => 'Tanjung Priok']);
        $this->districtModel->create(['id' => 443, 'city_id'=>43, 'province_id'=>6, 'name' => 'Penjaringan']);
        $this->districtModel->create(['id' => 442, 'city_id'=>43, 'province_id'=>6, 'name' => 'Pademangan']);
        $this->districtModel->create(['id' => 441, 'city_id'=>43, 'province_id'=>6, 'name' => 'Koja']);
        $this->districtModel->create(['id' => 440, 'city_id'=>43, 'province_id'=>6, 'name' => 'Kelapa Gading']);
        $this->districtModel->create(['id' => 439, 'city_id'=>43, 'province_id'=>6, 'name' => 'Jakarta Utara']);

        $this->cityModel->create(['id' => 45, 'province_id'=>6, 'name' => 'Kota Administrasi Jakarta Selatan']);
        $this->districtModel->create(['id' => 448, 'city_id'=>45, 'province_id'=>6, 'name' => 'Kebayoran Baru']);
        $this->districtModel->create(['id' => 447, 'city_id'=>45, 'province_id'=>6, 'name' => 'Jagakarsa']);
        $this->districtModel->create(['id' => 449, 'city_id'=>45, 'province_id'=>6, 'name' => 'Kebayoran Lama']);
        $this->districtModel->create(['id' => 446, 'city_id'=>45, 'province_id'=>6, 'name' => 'Jakarta Selatan']);
        $this->districtModel->create(['id' => 456, 'city_id'=>45, 'province_id'=>6, 'name' => 'Cilandak']);
        $this->districtModel->create(['id' => 455, 'city_id'=>45, 'province_id'=>6, 'name' => 'Tebet']);
        $this->districtModel->create(['id' => 450, 'city_id'=>45, 'province_id'=>6, 'name' => 'Mampang Prapatan']);
        $this->districtModel->create(['id' => 454, 'city_id'=>45, 'province_id'=>6, 'name' => 'Setiabudi']);
        $this->districtModel->create(['id' => 453, 'city_id'=>45, 'province_id'=>6, 'name' => 'Pasanggrahan']);
        $this->districtModel->create(['id' => 452, 'city_id'=>45, 'province_id'=>6, 'name' => 'Pasar Mingu']);
        $this->districtModel->create(['id' => 451, 'city_id'=>45, 'province_id'=>6, 'name' => 'Pancoran']);


        $this->cityModel->create(['id' => 42, 'province_id'=>6, 'name' => 'Kota Administrasi Jakarta Timur']);
        $this->districtModel->create(['id' => 460, 'city_id'=>42, 'province_id'=>6, 'name' => 'Cipayung']);
        $this->districtModel->create(['id' => 459, 'city_id'=>42, 'province_id'=>6, 'name' => 'Cakung']);
        $this->districtModel->create(['id' => 458, 'city_id'=>42, 'province_id'=>6, 'name' => 'Jakarta Timur']);
        $this->districtModel->create(['id' => 467, 'city_id'=>42, 'province_id'=>6, 'name' => 'Pasar Rebo']);
        $this->districtModel->create(['id' => 464, 'city_id'=>42, 'province_id'=>6, 'name' => 'Kramat Jati']);
        $this->districtModel->create(['id' => 468, 'city_id'=>42, 'province_id'=>6, 'name' => 'Pulo Gadung']);
        $this->districtModel->create(['id' => 463, 'city_id'=>42, 'province_id'=>6, 'name' => 'Jatinegara']);
        $this->districtModel->create(['id' => 466, 'city_id'=>42, 'province_id'=>6, 'name' => 'Matraman']);
        $this->districtModel->create(['id' => 461, 'city_id'=>42, 'province_id'=>6, 'name' => 'Ciracas']);
        $this->districtModel->create(['id' => 465, 'city_id'=>42, 'province_id'=>6, 'name' => 'Makassar']);
        $this->districtModel->create(['id' => 462, 'city_id'=>42, 'province_id'=>6, 'name' => 'Duren Sawit']);

        $this->districtModel->create(['id' => 0, 'city_id'=>0, 'province_id'=>0, 'name' => 'Luar Jakarta']);
//
//        $this->surveyorModel->create(['id' => 1, 'name'=> 'UPIN', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 41, 'district_id' => 427,
//            'latitude' => -6.186486,
//            'longitude' => 106.834091]);
//        $this->surveyorModel->create(['id' => 2, 'name'=> 'UPIN2', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 41, 'district_id' => 427,
//            'latitude' => -6.186486,
//            'longitude' => 106.834091]);
//
//        $this->surveyorModel->create(['id' => 3, 'name'=> 'IPIN', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 40, 'district_id' => 427,
//            'latitude' => -6.168329,
//            'longitude' => 106.758849]);
//        $this->surveyorModel->create(['id' => 4, 'name'=> 'IPIN2', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 40, 'district_id' => 427,
//            'latitude' => -6.168329,
//            'longitude' => 106.758849]);
//
//        $this->surveyorModel->create(['id' => 5, 'name'=> 'IHSAN', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 43, 'district_id' => 427,
//            'latitude' => -6.138414,
//            'longitude' => 106.863956]);
//        $this->surveyorModel->create(['id' => 6, 'name'=> 'IHSAN2', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 43, 'district_id' => 427,
//            'latitude' => -6.138414,
//            'longitude' => 106.863956]);
//
//        $this->surveyorModel->create(['id' => 7, 'name'=> 'MAIL', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 45, 'district_id' => 427,
//            'latitude' => -6.261493,
//            'longitude' => 106.810600]);
//        $this->surveyorModel->create(['id' => 8, 'name'=> 'MAIL2', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 45, 'district_id' => 427,
//            'latitude' => -6.261493,
//            'longitude' => 106.810600]);
//
//        $this->surveyorModel->create(['id' => 9, 'name'=> 'JARJIT', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 42, 'district_id' => 427,
//            'latitude' => -6.225014,
//            'longitude' => 106.900447]);
//        $this->surveyorModel->create(['id' => 10, 'name'=> 'JARJIT2', 'phone_number' => '08123123123',
//'email' => 'dummy@mail.com', 'max_survey' => 5,
//            'province_id' => 6, 'city_id' => 42, 'district_id' => 427,
//            'latitude' => -6.225014,
//            'longitude' => 106.900447]);
//

    }
}