<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * ClientsTableSeeder constructor.
     */
    public function __construct()
    {
        Client::truncate();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Client::class)->create();
    }
}
