<?php

use Illuminate\Database\Seeder;

class MerchantsSeeder extends Seeder
{
    private $merchantModel;
    private $addressModel;
    private $userModel;
    private $bankModel;
    private $roleModel;

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {
        $this->merchantModel = new \App\models\Merchant;
        $this->userModel = new \App\models\User;
        $this->roleModel = new \App\models\Role;
        $this->addressModel = new \App\models\Address;
        $this->bankModel = new \App\models\Bank;
    }

    public function run()
    {
        $file = File::get('database/json/merchant_decrypted.json');
        $objects = json_decode($file);

        $admin = $this->userModel->first();
        $role = $this->roleModel->where('slug', 'merchant')->first();

        foreach ($objects as $object) {
            $address = $this->addressModel->create([
                'address' => $object->alamat_merchant,
            ]);

            $user = $this->userModel->create([
                'name'      => $object->nama_merchant,
                'email'     => $object->email_merchant,
                'password'  => bcrypt($object->decrypted)
            ])->user_role()->create([
                'role_id'   => $role->id
            ]);

            $this->merchantModel->create([
                'id' => $object->id,
                'user_id' => $user->id,
                'phone' => $object->phone_merchant,
                'address_id' => $address->id,
                'bank_id' => $object->bank_id,
                'password' => bcrypt($object->decrypted),
                'created_by' => $admin->id
            ]);
        }
    }
}
