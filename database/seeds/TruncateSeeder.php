<?php

use Illuminate\Database\Seeder;

class TruncateSeeder extends Seeder
{
    public function __construct()
    {
        $this->exceptModels = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (scandir(app_path('Models')) as $file) {
            if (is_file(app_path('Models/'.$file))) {
                $fileModel = str_replace('.php', '', $file);
                if (!in_array($fileModel, $this->exceptModels)) {
                    $classModel = '\\App\Models\\'.$fileModel;
                    DB::statement('TRUNCATE TABLE ' . (new $classModel)->table . ' RESTART IDENTITY CASCADE;');
//                    DB::statement('ALTER SEQUENCE ' . (new $classModel)->table . '_pkey RESTART WITH 1');
                }
            }
        }
    }
}
