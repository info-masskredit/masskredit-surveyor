<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TruncateSeeder::class);
        $this->call(UsersAccessSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(DummyLocationSeeder::class);
    }
}
