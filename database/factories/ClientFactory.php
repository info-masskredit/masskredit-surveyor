<?php
/**
 * Created by PhpStorm.
 * User: irwaryanto
 * Date: 16/07/18
 * Time: 17:35
 */
$factory->define(\Laravel\Passport\Client::class, function () {
    return [
        'user_id' => null,
        'name' => config('app.name').' Password Grant Client',
        'secret' => 'LkPvLe5VHay2uh785uyHJTuOeyu3O0FNhglYxJp4',
        'redirect' => 'http://localhost',
        'personal_access_client' => false,
        'password_client' => true,
        'revoked' => false,
    ];
});
