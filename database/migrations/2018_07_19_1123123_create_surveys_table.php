<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function(Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('user_name')->nullable();
            $table->string('user_nik')->nullable();
            $table->string('user_employment')->nullable();
            $table->string('user_phone_number')->nullable();
            $table->string('user_work_period')->nullable();
            $table->string('user_placement_location')->nullable();
            $table->string('company_hrd_name')->nullable();
            $table->string('company_industry')->nullable();
            $table->string('company_call_number')->nullable();
            $table->string('company_bank_payroll')->nullable();
            $table->string('interviewee_name')->nullable();
            $table->string('interviewee_employment')->nullable();
            $table->string('status_name')->nullable();
            $table->integer('surveyor_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable()->unsigned();
            $table->integer('province_id')->nullable()->unsigned();
            $table->integer('city_id')->nullable()->unsigned();
            $table->integer('district_id')->nullable()->unsigned();
            $table->string('econ_relationship')->nullable();
            $table->string('position')->nullable();
            $table->text('proof_of_salary')->nullable();
            $table->double('work_location_lat')->nullable();
            $table->double('work_location_lon')->nullable();
            $table->string('work_location')->nullable();
            $table->text('photo_location')->nullable();
            $table->text('photo_interviewee')->nullable();
            $table->text('survey_empty_reason')->nullable();
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('econ_name')->nullable();
            $table->string('econ_phone')->nullable();
            $table->string('econ_address')->nullable();
            $table->boolean('is_offline')->nullable()->unsigned()->default(0)->comment('0 = Offline, 1 = Online');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveys');
    }
}
