<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveyors', function(Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->text('nik')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('province_id')->nullable()->unsigned();
            $table->integer('city_id')->nullable()->unsigned();
            $table->integer('district_id')->nullable()->unsigned();
            $table->integer('assigned_survey')->default(0);
            $table->integer('max_survey')->default(0);
            $table->text('access_token')->nullable();
            $table->text('login_date')->nullable();
            $table->text('expire_at')->nullable();
            $table->string('work_area')->nullable();
            $table->integer('surveyed_today')->default(0);
            $table->integer('unsurveyed_today')->default(0);
            $table->integer('total_survey_today')->default(0);
            $table->string('fcm_token')->nullable()->comment('Token used for push notification');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('created_by')->nullable()->unsigned()->comment('0 = SYSTEM');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveyors');
    }
}
