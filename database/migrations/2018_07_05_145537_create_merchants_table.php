<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->integer('address_id')->nullable()->unsigned();
            $table->integer('bank_id')->nullable()->unsigned();
            $table->boolean('active')->nullable()->unsigned()->default(1)->comment('0 = Not Active, 1 = Active');
            $table->integer('created_by')->nullable()->unsigned()->comment('0 = SYSTEM');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('merchants');
    }
}
